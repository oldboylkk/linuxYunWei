# Linux命令总结

## pwd

```
pwd - print name of current/working directory
打印当前所在目录
查看当前所在目录
```

![1557230825660](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557230825660.png)

## ip  add 同 ip a

```
pwd - print name of current/working directory
IP-显示/操作路由、设备、策略路由和隧道 显示ip地址等详细信息
查看ip地址
```

![1557230872057](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557230872057.png)

## cd 切换目录

cd 

```
Change the current directory to DIR
改变目录 想去哪里 cd 哪里
```

cd ..

```
返回上一层目录
```



![1557231079381](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557231079381.png)

cd .

```
当前目录
```

![1557231118096](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557231118096.png)

cd -

```
在最近两次工作的目录来回切换
```

![1557231204162](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557231204162.png)

cd /

```
回根目录
```

![1557231265719](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557231265719.png)

## tree 以树形显示目录结构

tree  

```
tree - list contents of directories in a tree-like format.
以树型格式列出目录的内容。
```



![1557232306640](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557232306640.png)

tree -d

```
只显示目录
```

![1557232412934](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557232412934.png)

tree -L

```
显示多少层目录
```

![1557232476560](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557232476560.png)

## mkdir 创建目录

mkdir

```
 mkdir - make directories
 生成目录 创建目录
```



![1557232686341](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557232686341.png)

mkdir -p 

```
递归创建目录
```

![1557233025228](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557233025228.png)

mkdir {}

```
序列创建目录
```

![1557233757685](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557233757685.png)

mkdir{dir,yy,uu}

```
创建不连续目录时,用逗号隔开
```

![1557233909885](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557233909885.png)

## touch 创建文件

```
touch - change file timestamps
创建文件或更改文件时间戳
```

touch 

![1557234146170](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557234146170.png)

touch 生成序列文件{}

![1557234316759](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557234316759.png)

touch 修改时间

![1557234346147](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557234346147.png)

![1557234484364](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557234484364.png)

## ls 显示目录下内容

```
ls - list directory contents
列出目录内容
显示目录下内容及属性信息的命令
```

ls

![1557234742525](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557234742525.png)

ls -a

```
查看目录下隐藏文件
```

![1557234788654](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557234788654.png)

ll 

```
显示目录下详细信息
```

![1557234873202](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557234873202.png)

ls -r

```
逆序 倒序排序
```

ls -t

```
根据最后修改时间排序,默认是以文件名排序,通常与-l 连用
```

![1557235132685](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557235132685.png)

![1557235156043](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557235156043.png)

## cp 复制文件或者目录

cp

```
cp - copy files and directories
复制文件或者目录
```



![1557235405147](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557235405147.png)

cp -r

```
递归式复制目录 即复制目录下的所有层级的子目录及文件
```

![1557235804957](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557235804957.png)

cp -p 

```
复制的时候保持属性不变
```

cp -d

```
复制的时候保持软连接(快捷方式)
```

cp -a

```
-a 等于-pdr
```

## mv  移动文件或者目录

mv

```
mv - move (rename) files
移动文件或目录 可重命名
```



![1557238160855](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557238160855.png)

## rm 删除目录或文件

rm  等同 rm -i

```
rm - remove files or directories
删除文件或目录
默认询问是否删除文件
```

![1557238377121](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557238377121.png)

rm -f 

```
强制删除不提示
```

![1557238688381](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557238688381.png)

rm -r

```
递归删除
```

![1557239184510](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557239184510.png)

## echo 可重定向 可追加

echo > 重定向 当文件不存在时 可自动创建文件

```
echo - display a line of text
echo-显示一行文本
将 echo的内容输出到屏幕
```



![img](file:///C:\Users\86186\AppData\Roaming\Tencent\Users\1459467588\QQ\WinTemp\RichOle\VTW}]B2SHQGQA{EQ0HZAV9W.png)

echo >> 追加 在行尾追加

![1557240744717](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557240744717.png)

## vim 快捷键 

```
vim - Vi IMproved, a programmers text editor
改进了vim-vi，一个程序员文本编辑器 文本编辑器
```



按 a i 进入编辑模式 insert

![1557241559397](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557241559397.png)

esc 退出编辑模式 进入命令行模式

![1557241638965](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557241638965.png)

输入:wq 保存退出

![1557241727104](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557241727104.png)

编辑模式下显示行号

![1557242453087](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557242453087.png)

vim底行模式搜搜索  用/  输入你想搜做的内容 搜索出来的内容会高亮显示

![1557244201288](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557244201288.png)

![1557244217253](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557244217253.png)

## cat  查看文件

```
cat - concatenate files and print on the standard output
查看文件 将文件内容显示到屏幕
```



cat -n 文件名  查看文件显示行号

![1557242315572](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557242315572.png)

## find 查找文件

```
find 从哪找 找什么类型 参数 名字
find / -type f -name"文件名" 文件用f 目录用d
```

![1557243516723](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557243516723.png)