linux学习

## day01

### 1.计算机基础

运维人员的职责:

1. 7*24是服务器稳定运行
2. 数据不能丢失损坏
3. 提升用户体验

常见的服务器种类

- DELL

  |   DELL    |         1U          |    2U     |
  | :-------: | :-----------------: | :-------: |
  |   2010    |      1850/1950      | 2850/2950 |
  | 2010-2013 |      R410/R610      |   R710    |
  | 2014-2016 | R420/R430/R620/R630 | R720/R730 |
  |   2018    |                     |   R740    |

- IBM

  |       1U        |  2U  |  3U  |  4U  |
  | :-------------: | :--: | :--: | :--: |
  | 3550/M3 3550/M5 | 3650 | 3850 | 3950 |

- HP

  DL388 Gen10 32G ddr4

  DL388 Gen9 16G ddr4

- 联想

  SR650 16G DDR4 CPU一颗

  x3650 16G DDR4 CPU一颗

  SR550 16G DDR4 CPU一颗

- 浪潮等等(别的需要自己了解)

  NF5280M5系列 16G DDR4 

服务器厚度用unit来表示 1U 2U 服务器高度

1U两个网口

2U四个网口

服务器种类分为:

- 机架式
- 塔式(在IDC存储占地方)
- 刀片式

服务器组成

- 电源
  - 双电源(双电路供电,民用电,商业用电,1U的电源瓦数为550W,2U的瓦数为750W)
  - UPS蓄电池 30min-1h
  - 柴油发电机
- cpu 
  - 负责运算和控制
  - cpu风扇负责散热(关键)
  - cpu品牌intel xeon
- 内存
  - cpu和磁盘之前的缓冲设备
  - 程序:静态的 放在硬盘里边的视频 图片
  - 进程:播放视频 运行中
  - 守护进程:不间断运行在后台 或成为服务
- 磁盘
  - 永久存放数据的地方
  - 磁盘的接口
    - IDE SCSI(淘汰)
    - SATA(机械) 7200-7500转速 优点:容量大 价格便宜 缺点:速度慢
    - SAS 10500转速
    - SSD(固态) 优点:速度快 缺点:容量小 价格贵
  - 磁盘的类型:机械磁盘和SSD固态硬盘
  - 性能与价格:SSD>SAS>SATA
  - 冷数据用SATA 热点数据用SSD

RAID待补充

RAID0

![img](https://images2015.cnblogs.com/blog/576154/201611/576154-20161124222640518-2080403622.png)

定义：

RAID 0又称为Stripe或Striping，它代表了所有RAID级别中最高的存储性能。RAID 0提高存储性能的原理是把连续的数据分散到多个磁盘上存取，这样，系统有数据请求就可以被多个磁盘并行的执行，每个磁盘执行属于它自己的那部分数据请求。这种数据上的并行操作可以充分利用总线的带宽，显著提高磁盘整体存取性能。

工作原理：

系统向三个磁盘组成的逻辑硬盘（RAID0 磁盘组）发出的I/O数据请求被转化为3项操作，其中的每一项操作都对应于一块物理硬盘。通过建立RAID 0，原先顺序的数据请求被分散到所有的三块硬盘中同时执行。从理论上讲，三块硬盘的并行操作使同一时间内磁盘读写速度提升了3倍。 但由于总线带宽等多种因素的影响，实际的提升速率肯定会低于理论值，但是，大量数据并行传输与串行传输比较，提速效果显著显然毋庸置疑。

优缺点：

读写性能是所有RAID级别中最高的。

RAID 0的缺点是不提供数据冗余，因此一旦用户数据损坏，损坏的数据将无法得到恢复。RAID0运行时只要其中任一块硬盘出现问题就会导致整个数据的故障。一般不建议企业用户单独使用。

总结：

磁盘空间使用率：100%，故成本最低。

读性能：N*单块磁盘的读性能

写性能：N*单块磁盘的写性能

冗余：无，任何一块磁盘损坏都将导致数据不可用。

 

RAID1

![img](https://images2015.cnblogs.com/blog/576154/201611/576154-20161124223115643-890801266.jpg)

定义：

RAID 1通过磁盘数据镜像实现数据冗余，在成对的独立磁盘上产生互为备份的数据。当原始数据繁忙时，可直接从镜像拷贝中读取数据，因此RAID 1可以提高读取性能。RAID 1是磁盘阵列中单位成本最高的，但提供了很高的数据安全性和可用性。当一个磁盘失效时，系统可以自动切换到镜像磁盘上读写，而不需要重组失效的数据。

工作原理：

RAID1是将一个两块硬盘所构成RAID磁盘阵列，其容量仅等于一块硬盘的容量，因为另一块只是当作数据“镜像”。RAID1磁盘阵列显然是最可靠的一种阵列，因为它总是保持一份完整的数据备份。它的性能自然没有RAID0磁盘阵列那样好，但其数据读取确实较单一硬盘来的快，因为数据会从两块硬盘中较快的一块中读出。RAID1磁盘阵列的写入速度通常较慢，因为数据得分别写入两块硬盘中并做比较。RAID1磁盘阵列一般支持“热交换”，就是说阵列中硬盘的移除或替换可以在系统运行时进行，无须中断退出系统。RAID1磁盘阵列是十分安全的，不过也是较贵一种RAID磁盘阵列解决方案，因为两块硬盘仅能提供一块硬盘的容量。RAID1磁盘阵列主要用在数据安全性很高，而且要求能够快速恢复被破坏的数据的场合。

在这里，需要注意的是，读只能在一块磁盘上进行，并不会进行并行读取，性能取决于硬盘中较快的一块。写的话通常比单块磁盘要慢，虽然是并行写，即对两块磁盘的写入是同时进行的，但因为要比较两块硬盘中的数据，所以性能比单块磁盘慢。

优缺点：

RAID1通过硬盘数据镜像实现数据的冗余，保护数据安全，在两块盘上产生互为备份的数据，当原始数据繁忙时，可直接从镜像备份中读取数据，因此RAID1可以提供读取性能。
RAID1是硬盘中单位成本最高的，但提供了很高的数据安全性和可用性，当一个硬盘失效时，系统可以自动切换到镜像硬盘上读/写，并且不需要重组失效的数据。

总结：

磁盘空间使用率：50%，故成本最高。

读性能：只能在一个磁盘上读取，取决于磁盘中较快的那块盘

写性能：两块磁盘都要写入，虽然是并行写入，但因为要比对，故性能单块磁盘慢。

冗余：只要系统中任何一对镜像盘中有一块磁盘可以使用，甚至可以在一半数量的硬盘出现问题时系统都可以正常运行。

 

RAID 5

![img](https://images2015.cnblogs.com/blog/576154/201611/576154-20161124225904331-1146819836.jpg)

 

定义：

RAID 5是RAID 0和RAID 1的折中方案。RAID 5具有和RAID0相近似的数据读取速度，只是多了一个奇偶校验信息，写入数据的速度比对单个磁盘进行写入操作稍慢。同时由于多个数据对应一个奇偶校验信息，RAID5的磁盘空间利用率要比RAID 1高，存储成本相对较低，是目前运用较多的一种解决方案。

工作原理：

RAID5把数据和相对应的奇偶校验信息存储到组成RAID5的各个磁盘上，并且奇偶校验信息和相对应的数据分别存储于不同的磁盘上，其中任意N-1块磁盘上都存储完整的数据，也就是说有相当于一块磁盘容量的空间用于存储奇偶校验信息。因此当RAID5的一个磁盘发生损坏后，不会影响数据的完整性，从而保证了数据安全。当损坏的磁盘被替换后，RAID还会自动利用剩下奇偶校验信息去重建此磁盘上的数据，来保持RAID5的高可靠性。

做raid 5阵列所有磁盘容量必须一样大，当容量不同时，会以最小的容量为准。 最好硬盘转速一样，否则会影响性能，而且可用空间=磁盘数n-1，Raid 5 没有独立的奇偶校验盘，所有校验信息分散放在所有磁盘上， 只占用一个磁盘的容量。

总结：

磁盘空间利用率：(N-1)/N，即只浪费一块磁盘用于奇偶校验。

读性能：(n-1)*单块磁盘的读性能，接近RAID0的读性能。

写性能：比单块磁盘的写性能要差（这点不是很明白，不是可以并行写入么？）

冗余：只允许一块磁盘损坏。

 

RAID10

![img](https://images2015.cnblogs.com/blog/576154/201611/576154-20161124232438628-1448311671.jpg)

定义：

RAID10也被称为镜象阵列条带。象RAID0一样，数据跨磁盘抽取；象RAID1一样，每个磁盘都有一个镜象磁盘, 所以RAID 10的另一种会说法是 RAID 0+1。RAID10提供100%的数据冗余，支持更大的卷尺寸，但价格也相对较高。对大多数只要求具有冗余度而不必考虑价格的应用来说，RAID10提供最好的性能。使用RAID10，可以获得更好的可靠性，因为即使两个物理驱动器发生故障（每个阵列中一个），数据仍然可以得到保护。RAID10需要4 + 2*N 个磁盘驱动器（N >=0)， 而且只能使用其中一半(或更小, 如果磁盘大小不一)的磁盘用量, 例如 4 个 250G 的硬盘使用RAID10 阵列， 实际容量是 500G。

实现原理：

Raid10其实结构非常简单，首先创建2个独立的Raid1，然后将这两个独立的Raid1组成一个Raid0，当往这个逻辑Raid中写数据时，数据被有序的写入两个Raid1中。磁盘1和磁盘2组成一个Raid1，磁盘3和磁盘4又组成另外一个Raid1;这两个Raid1组成了一个新的Raid0。如写在硬盘1上的数据1、3、5、7，写在硬盘2中则为数据1、3、5、7，硬盘中的数据为0、2、4、6，硬盘4中的数据则为0、2、4、6，因此数据在这四个硬盘上组合成Raid10，且具有raid0和raid1两者的特性。
虽然Raid10方案造成了50%的磁盘浪费，但是它提供了200%的速度和单磁盘损坏的数据安全性，并且当同时损坏的磁盘不在同一Raid1中，就能保证数据安全性。假如磁盘中的某一块盘坏了，整个逻辑磁盘仍能正常工作的。
当我们需要恢复RAID10中损坏的磁盘时，只需要更换新的硬盘，按照RAID10的工作原理来进行数据恢复，恢复数据过程中系统仍能正常工作。原先的数据会同步恢复到更换的硬盘中。

总结：

磁盘空间利用率：50%。

读性能：N/2*单块硬盘的读性能

写性能：N/2*单块硬盘的写性能

冗余：只要一对镜像盘中有一块磁盘可以使用就没问题。



## day03

### 1.安装Centos

1. 需要安装什么操作系统
2. 操作系统去哪里下载 www.centos.org

### 2.网卡名称

1. centos5 6 eth0
2. centos7 ens33

### 3.安装虚拟机步骤内容

1. LVM 逻辑卷管理
2. 磁盘分区
   1. /boot 存放引导程序的 存放内核的地方 200M
   2. swap 交换分区 内存<8 内存的1.5倍 3 4.5     内存>=8 8G 可动态调整
   3. / 称为根

### 4.网络连接不上的原因

1. 创建虚拟机的时候,网络配置不对.

2. 检查虚拟网络编辑器中的-子网,掩码,NAT设置

   保证网络连通的一致性

3. 查看windows中的VM相关服务

   重启相关服务,防治僵尸进程

4. 各种疑难杂症

## day04

### 1. SSH连接排错

1. IP地址
   - 局域网
     - 私网IP 例如:192.168.19.200
   - 虚拟机
     - 私网IP 例如:VMware 里边的虚拟机 10.0.0.200
   - 公网ip
     - 需要上网用的 服务器的ip地址
     - IDC机房 公网IP
     - 阿里云 公网IP

### 2.端口 22      (10.0.0.200)(ping 10.0.0.200) 如果通为正常 超时不正常

以下为小故事更形象

屌丝(运维)去洗浴中心之路

1. 10.0.0.200 (洗浴中心地址)

2. 劫财劫色(firewalld  NetworkManager  SElinux防火墙)(暂时不用)

   systemctl stop Network Manager

   systemctl disable NetworkManager

3. 搓澡    拔罐    按摩    桑拿    (特殊服务)

   80        443     8080   6379      22

4. telnet 10.0.0.200  22 端口对应服务 虚拟机排查:ip add 要熟练掌握

   - 首先查看路通不通 ping 10.0.0.200

     - ping不通

       - 首先查看网卡ip地址是否正常

         - 是否有ip地址 是否是24位 是否为up状态

       - 在虚拟机中ping网关

         - 如何查看网关

           cat /etc/sysconfig/network-scripts/ifcfg-eth0

           GATEWAY=10.0.0.254

           查看网关配置否是正确

           编辑-虚拟网络编辑器-vmnet8-net设置 是否和你配置的相同

         - 查看虚拟网络编辑器是否有vmnet8 如果没有 关闭虚拟机 以管理员身份运行

       - 查看本地网络连接 查看VMware8 IP地址是否10.0.0.*同一网段(前三段必须相同)

         - 如果网段(前三段)不一样:

           1.编辑-虚拟网络编辑器-还原默认设置（你配置的10.0.0.0 255.255.255.0没啦，重新配置）还原前需关闭虚拟机（检查IP地址 systemctl restart network）

           2.固定IP地址 打开我的电脑 输入网络连接 找到vmnet8-属性-双击协议版本4-使用下面的IP地址 IP地址 10.0.0.1 子网掩码 255.255.255.0

   - 测试端口是否打开 telnet 10.0.0.200 22

5. 虚拟机上不去网:

   - ping 网关 10.0.0.254
     - ping不同 查看网关地址 网卡配置文件 配置错了
     - cat /etc/sysconfig/network-scripts/ifcfg-eth0
     - 如果不对 更改地址即可
     - 编辑-虚拟网络编辑器 -vmnet8-net设置-网关(必须和你网卡配置文件网关相同)
   - ping 公网IP地址1.2.4.8    8.8.8.8    114.114.114.114
     - 检查DNS是否能ping通
     - cat /etc/sysconfig/network-scripts/ifcfg-eth0
     - DNS=1.2.4.8
     - 检查cat /etc/resolv.conf 文件你如果是空的
       - systemtcl restart network
       - 手动配置 vi /etc/resolv.conf
         - nameserver 1.2.4.8

6. 缺少关键文件 /var/empty/sshd

   ```
   systemctl restart sshd
   ```

   

## day05
### 1.linux命令行组成结构

| 命令 |      |    条件/参数    |      | 对象/文件/目录  |
| :--: | :--: | :-------------: | :--: | :-------------: |
| 结婚 | 空格 | -有房有车有存款 | 空格 |     白富美      |
| 结婚 | 空格 | -没车有房有存款 | 空格 |  是个女的就行   |
|  rm  | 空格 |       -f        | 空格 | /tep/oldboy.txt |

```
[root@lodboy~]#	#<-这是超级管理员root用户对应的命令行
[root@lodboy~]$	#<-这是普通用户 oldboy对应的命令行
其中root==>代表当前登录的用户 
	@	分割符
	oldboy	代表当前主机名
	~	表示用户当前所在路径
	#	表示root管理员提示符
	$	表示普通用户提示符
[root		@		oldboy		~]		#
当前用户    分隔符		主机名		当前路径  提示符
```

### 2.关机命令

```
重启命令
shutdown -r 10		#10分钟之后重启
shutdown -r 0		#立刻重启
shutdown -r now		#立刻重启

关机命令
shutdown -h 10		#10分钟之后关机
shutdown -h 0		#立刻关机
shutdown -h now		#立刻关机

取消正在进行的关机或重启命令
shutdown -c

重启
reboot

关机
poweroff
halt
```

### 3.关机,重启和注销的命令列表

|      命令       |                       说明                       |
| :-------------: | :----------------------------------------------: |
|  **关机命令**   |                                                  |
| shutdown -h now |                立刻关机(生产常用)                |
| shutdown -h +1  |        1分钟以后关机 也可以是时间点 11:00        |
|      halt       | 立即停止系统,需要人工关闭电源,是reboot的链接文件 |
|     init 0      |            切换运行级别到0,0表示关机             |
|    poweroff     |             立即停止系统,并关闭电源              |
|  **重启命令**   |                                                  |
|     reboot      |                立即重启(生产常用)                |
| shutdown -r now |                立即重启(生产常用)                |
| shutdown -r +1  |                  1分钟以后重启                   |
|     init 6      |            切换运行级别到6,6表示重启             |
|**注销命令**|                                                  |
| logout | 注销退出当前用户窗口 |
| exit | 注销退出当前用户窗口,快捷键Ctrl+d |

### 4.linux命令行常用快捷键

| 快捷键 |         执行命令意思         |
| :----: | :--------------------------: |
| ctrl+c |     cancel 取消当前操作      |
| ctrl+l |          clear 清屏          |
| ctrl+d |         退出当前用户         |
| ctrl+r |         查找历史命令         |
| ctrl+a |       把光标移动到首位       |
| ctrl+e |       把光标移动到最后       |
| ctrl+u | 把光标到行首的内容删除/剪切  |
| ctrl+y |             粘贴             |
| delete | 把光标所在处从前往后删除内容 |
| ctrl+k | 把光标到行尾的内容删除/剪切  |
| ctrl+→ |       向右移动一个单词       |
| ctrl+← |       向左移动一个单词       |
| ctrl+s |             锁屏             |
| ctrl+q |             解锁             |

### 5.linux 文件及目录核心命令

1. pwd:显示当前所在的位置信息

   ```
   pwd		查看当前所在的目录	此命令一般直接使用
   ```

2. cd:切换目录

   ```
   cd 切换目录
   cd 的常用选项:
   cd ~ 切换到当前用户的家目录
   cd . 保持当前目录不变
   cd .. 切换到上级目录
   cd - 在最近的两次工作目录之前来回切换*
   cd / 直接切换到/目录
   ```

3. tree:以树形结构显示目录下内容

   ```
   tree 命令语法:
   tree 选项 目录
   tree 以树形图列出文件目录结构
   tree 常用选项:
   -d 只显示目录
   -L 遍历牡蛎的最大层数,-L后加大于0的正整数 
   -F 在条目后加上文件类型的指示符号(*,/,=,@,|,其中一个)
   安装tree的命令: yum install tree -y
   ```

4. mkdir 创建目录

   ```
   mkdir 创建目录
   mkdir 常用选项:
   -p 递归常目录 可以连续创建多个目录
   -m 设置新目录默认对用的权限
   创建目录;
   mkdir{1..3}加花括号创建连续目录,用..隔开 花括号内可以是连续数字,连续字母
   注:适用于创建不连续的目录
   mkdir{dir,yy,uu}创建不连续目录时,用逗号隔开
   ```

5. touch:创建文件或更改文件时间戳

   ```
   touch 创建文件 修改文件时间
   {} 生成序列
   {1..10}
   {01..10}
   {a..z}
   ```

6. ls:显示目录下内容及属性信息的命令

   ```
   ls 查看当前目录下内容
   ls常用选项:
   -a 显示置顶目录下所有子目录与文件,包括隐藏文件
   -l 以列表方式显示文件的详细信息
   -h 配合 -l 以人性化方式显示文件大小,以K M G 为单位
   -t 根据最后修改时间排序,默认是以文件名排序,通常与-l 连用
   -F 在条目后加上文件类型的只是符号(*,/,=,@,|,其中一个)注:可以标识文件类型
   -d 显示目录本身的信息 而不是显示目录的内容
   -r 逆序 倒序排序 W
   ls-lt 按照时间进行排序
   ls-lrt 找出最新的文件
   -i 显示索引节点信息
   --full-time 以完整的时间格式输出
   ```

7. cp:复制文件或者目录

   ```
   cp 命令语法:
   cp 源文件 目标文件
   cp 复制文件或目录
   cp 常用选项:
   -r 递归式复制目录 即复制目录下的所有层级的子目录及文件
   -p 复制的时候保持属性不变
   -d 复制的时候保持软连接(快捷方式)
   -a 等于-pdr
   ```

## day06

### 1.echo "I am studying linux" 把内容输出到屏幕

```
> 重定向 先清空在写入 如果文件不存在 直接创建文件
>> 追加重定向 在行尾插入内容
{1..10} {a..z}{A..Z}
```

### 2.mv move移动

```
mv 文件或目录 要放到哪里
mv oldboy.txt test.txt	#修改了文件名
```

### 3.rm 删除目录或文件 慎用 工作中使用mv代替rm

```
-f 强制删除 不提示
-r 递归删除
-i 是否交互(提示是否删除)
```

### 4.vim快捷键

```
echo 可重定向 可追加
第一个里程碑 打开文件
vim test.txt
第二个里程碑 写入内容
i am studying linux
i insert 进入编辑模式 
esc 进入底行模式
:wq 保存并退出
```

### 5.命令行模式快捷键:

```
h 左移一个字符 
j 下移一行
k 上移一行
l 右移一个字符
G 移动到缓冲区的最后一行
gg 移动到缓冲区第一行
2200G 移动到缓冲区中第2200行 #2200 是数字可变
x 删除当前光标坐在位置的字符
dd 删除当前光标所在行
2dd 删除两行
u 撤销上次执行的命令
dG 删除全部光标后
D 快速删除光标到行尾
C 快速删除光标到行尾并进入编辑模式
^ 移动到行首 Home
$ 移动到行尾 End
↑ ↓ ← → 光标移动
yy 复制一行内容	(3yy 复制光标以下三行)
p 粘贴内容
d0 删除光标前到行首的内容
o 在光标所在列下新增一列并进入输入模式
O 在光标所在列上方新增一列并进入输入模式
dw 删除一个单词


1move10 把第一行剪切到第十行的后面 简写mo
1copy10 把第一行复制到第十行的后面 简写co

:%s/root/oldboy/g
%文本内全局替换
不加 g 只查找替换行内第一个匹配到内容 加上g行内所有匹配到内容替换
ZZ 保存退出
```

### 6.cat 查看文件内容

```
-n 查看文件总行数 cat -n 文件
-b 不统计空行  就是文件中有空行然后不统计
```

### 7.编辑模式显示行号

```
:set nu 
```

### 8.查找文件名

```
find / -type f -name"文件名"
```

## day07

### 1.vim底行模式搜搜索  用/  输入你想搜做的内容

1. n查找下一个 N往上查找 查找上一个
2. q! 强制退出不保存
3. wq 保存并退出
4. wq! 强制保存退出

```
vim -r 文件名(有交换文件的时候使用,需要保存的时候)
```

### 2.vim 不正常退出 会产生隐藏文件.swp

1. 只要使用vim 打开文件 则会产生一个隐藏文件.xxx.swp 正常退出则不会产生.xxx.swp
2. 如果不恢复新加入内容 直接删除.xxx.swp 删除后再编辑文件
3. 如果要恢复新加入内容 使用 vim -r 文件名 进入文件回车即可恢复,完成后删除隐藏文件.xxx.swp

### 3.目录结构

1. 一切从根开始
2. linux根目录下面的目录是一个有层次的树状结构
3. linux如果想访问所有设备,必须得有接口才可以使用
   1. 如果没有接口的设备 相当于没有窗户,没有门的 
   2. 如果想访问 必须给访问一个接口 挂载点

### 4.Centos下磁盘分区

|        分区前        |             分区后             |
| :------------------: | :----------------------------: |
| /dev/sda    第一块盘 | sda1<br />sda2<br />sad3<br /> |
| /dev/sdb    第二块盘 |                                |
|                      |                                |

1. 如何把光盘放入系统

   ```
   挂载:mount /dev/cdrom /mnt/
   卸载:umount -f /mnt/
   ```

## day08

### 1.目录结构 

|      目录      |                           目录含义                           |
| :------------: | :----------------------------------------------------------: |
|      bin       |                命令 二进制文件存放目录 binary                |
|      boot      |                       系统引导程序***                        |
|      dev       |         device 设备 光盘 硬盘 (磁盘挂载才能使用)***          |
|      etc       |                    存放系统的配置文件***                     |
|      home      |                     普通用户的家目录 ***                     |
| lib<br />lib64 |         libary库 库文件<br />lib64 系统是64位 库文件         |
|   lost+found   |             磁盘或文件系统损坏 断电 临时文件位置             |
|      mnt       |   临时的挂载点***<br />挂载点--目录--给设备提供了一个入口    |
|      opt       |                 option第三方软件都安装在这里                 |
|      proc      | 虚拟的目录 目录里边的内容是内存中信息<br />(内核 进程 软件) 为了解决一切皆文件 *** |
|      root      |                  皇宫 root 用户的家目录***                   |
|      sbin      |        super binary 超级命令 只有root用户能使用的命令        |
|    selinux     |               selinux 及他的配置文件存放的位置               |
|      sys       |                     虚拟的目录 内存信息                      |
|      tmp       |                temp 临时文件的存放位置 垃圾堆                |
|      usr       |                        存放用户的程序                        |
|      var       |               varlabla 经常变换的文件 系统日志               |
|                |                                                              |
|                |                                                              |

### 2.重要的目录  网卡配置文件

```
/etc/sysconfig/network-scripts/ifcfg-eth0
```

```
TYPE=Ethernet	#以太网
BOOTPROTO=none	#以什么方式获取ip地址 两种方式
#BOOTPROTO=DHCP #自动获取下发ip地址
#BOOTPROTO=static	#静态ip地址 手动配置的ip地址
NAME=eth0	#网卡名
DEVICE=eht0	#硬件网卡的名称 0代表了第一块网卡,1代表第二块网卡
ONBOOT=yes	#开机自动激活网卡
IPADDR=10.0.0.200	#IP地址
PREFIX=24	#子掩码225.225.225.0 #NETMASK=255.255.255.0
GATEWAY=10.0.0.254	#网关
DNS1=1.2.4.8
DNS2=114.114.114.114
```

### 3.DNS配置文件

```
/etc/resolv.conf
```

- 优先于网卡DNS 优先进行解析
  - DNS配置文件 如果DNS配置在网卡 重启网卡 则网卡DNS会覆盖resolv.conf
  - 网卡中配置:网卡中不配置DNS,只在/etc/resolv.conf中配置DNS 重启网卡不覆盖

## day09

### etc/  下目录结构内容

```
网卡配置文件
/etc/sysconfig/network-scripts/ifcfg-eth0
```

/etc/resolv.conf

```
/etc/resolv.conf
该配置文件必须有DNS服务器地址
优先于网卡DNS 优先进行就习
1.DNS配置文件
如果dns配置在网卡 重启网卡 则网卡的dns则会覆盖resolv.conf
2.网卡中配置
网卡中不配置dns 只在/etc/resolv.conf 中配置dns 重启网卡不覆盖
```

/etc/hosts 

```
/etc/hosts 
本地配置解析域名的
```

hostname 查看主机名称

```
hostname -I 查看或者获取IP地址
hostname 主机名称 临时生效 重启失效 临时配置 centos7 相同
/etc/sysconfig/network 永久配置主机名 (centos 6)
centos7 配置主机名方法
方法1:hostnamectl set-hostname oldboyedu 永久设置主机名 (centos7)重启系统生效
方法2:/etc/hostname centos7 主机名配置文件 重启系统生效
```

 /etc/fstab

```
 /etc/fstab （开机自动挂载）系统开机为我们的硬件和挂载点的对应关系
  按照配置文件的格式写入  开机自动挂载我们的设备
```

/etc/rc.local 

```
/etc/rc.local  	开机自动执行此配置文件中的内容（每次开机都会执行一遍）
重要的服务开机自动启动 （或者其他程序，包括命令都可执行）
centos6 直接写入即可生效
centos7 必须加一个执行权限 chmod +x /etc/rc.d/rc.local
```

/etc/profile 环境变量配置文件

```
什么是变量	变量存在于内存
name="刘阔阔" 把右边的值赋值给左边
输出变量
echo $name

1.都是大写
2.在任意的位置都可以使用
3.是系统已经定义好的
环境变量配置文件
相当于国法 全局配置文件 (针对所有的用户生效) 国法规定
/etc/profile
/etc/bashrc
家规 针对当前用户生效 (你当前登录的用户,只影响本身)
~/.bashrc
~/.bash_profile
```

/etc/bashrc 别名 函数

/etc/issue

```
用户登录系统前显示的内容,默认显示 操作系统版本 内核版本
/etc/issue.net
实际工作 需要清空 登录前不提示
清空文件的方法
方法一:vim 编辑 dG 从光标所在的处删除到文件底部
方法二:echo > /etc/issue 清空文件 > /etc/issue
```

/etc/motd 用户登录系统后显示的内容

/etc/redhat-release 查看当前os版本

/usr/local 编译安装的默认安装位置

&& 

```
makdir liukuokuo/ && touch /root/liukuokuo/lkk.txt
前面的命令执行成功  则执行后面的命令
如何在linux下同时执行两条命令
ifdown eth0 && ifup eth0
```

### Linux  下运行级别 ********

```
cat /etc/inittab 查看运行级别等级
centos6 启动级别  runlevel  查看运行级别
```

|                   centos6                    |  centos7 target  |
| :------------------------------------------: | :--------------: |
|             0 关闭 关闭操作系统              | poweroff.target  |
|        1 单用户模式 救援模式 没有网络        |  rescue.target   |
|          2 多用户模式 NFS 没有网络           |    multi-user    |
| 3 完全多用户 命令行 默认就是3 当前使用的级别 |    multi-user    |
|                4 保留 待开发                 |    multi-user    |
|  5 桌面图形化模式 需要安装desktop 才可运行   | graphical.target |
|           6 重启 重新启动操作系统            |  reboot.target   |

使用方式

1. 在命令行如何使用
   - init 6 直接重启 后边直接跟运行级别
2. 配置文件
   - /etc/inittab 默认级别3 可更改为其他级别进行重启

centos7 target

1. 查看运行级别
   - runlevel
     - ll /etc/systemd/default.target
2. 如何设置运行级别 /etc/inittab   centos7不生效了
   - systemctl get-default	获取默认的运行级别
   - systemctl set-default poweroff.target 设置默认的运行级别

### 软件安装 yum rpm 源码

方法一:

```
yum 最常用 缺什么安装什么
缺少依赖 解决依赖问题 少什么依赖yum自动安装什么依赖
yum install vim tree net-tools
-y 安装过程中 提示是否继续 一路yes
yum repolist 查看所有的安装包的个数
yum list 列出所有的安装包

删除卸载已安装软件:
yum remove 包名

1.yum 安装
     命令的名称  服务的名称  （包的名称）
	 yum list  列出所有没有安装过的安装包  可安装的
	 1.我要安装一个软件 名称为 wget
	 2.我搜索到这个报名
	   yum list|grep wget
	   grep 'xxxx' file  =====  cat file|grep 'xxxx'
	   ls|grep xxx
	 3.进行yum安装
	   yum -y install wget
	删除: 使用yum安装的软件包 建议以yum删除 yum会解决依赖问题 
		  yum 删除也会自动解决依赖问题
```

方法二:

```
rpm 不会自动解决依赖问题 需要自己安装依赖
rpm -ivh xxx.rpm
-i install
-v verbose	显示安装过程
-h 以人类可读的方式显示
rpm -qa	查看所有的安装包
q query 查看
a all 所有的
rpm -qa tree lrzsz 查看是否安装tree和lrzsz
rpm -ql  查询软件安装的所有的文件位置    详细信息
rpm -e 软件包全名 卸载软件
1.你要删除一个软件 名称为wget
2.搜索全名
rpm -qa|grep wget
```

方法三:

```
编译安装  使用rpm -qa 无法查询编译安装的软件
你想要什么功能 不想要什么功能  自定义  有选择的安装
1. 拿到源码
2. 编译安装
备菜/切菜      做菜          上菜     		  吃
./configure   make     make install    找到目录使用 rm -rf	
```

## day10

### 1./var  目录结构 variable 经常变换的文件 系统日志 

/var/log/messages 系统默认的日志文件

```
日志切割 默认10分钟切割 为了防止日志文件过大 控制在1G
```

/var/log/secure 系统安全的日志 记录了用户登录的过程信息

### 2./proc  目录结构 (内存映射)

/proc/cpuinfo

```
/proc/cpuinfo cpu信息 使用命令查看 lscpu
		processor	: 0  第几个核心
		[root@oldboyedu ~]# cat /proc/cpuinfo|grep 'processor'
		processor	: 0
		processor	: 1
		processor	: 2
		processor	: 3

		physical id	: 0  第几个CPU 几颗  几路
		[root@oldboyedu ~]# cat /proc/cpuinfo|grep 'physical id'
		physical id	: 0
		physical id	: 0
		physical id	: 1
		physical id	: 1
		CPU(s):                4
		On-line CPU(s) list:   0-3
		Thread(s) per core:    1
		Core(s) per socket:    2
		Socket(s):             2
```

/proc/meminfo 

```
/proc/meminfo 查看内存信息 使用命令查看 free -h
MemTotal:        1528412 kB		总共内存
MemFree:         1253588 kB		剩余内存
MemAvailable:    1229392 kB		可用内存
Buffers:            2076 kB		缓冲
Cached:            87272 kB		缓存
```

/proc/loadavg 

```
/proc/loadavg 
负载 衡量当前系统的一个繁忙程度
怎么衡量呢
数字达到你cpu核心的数量
cpu  4核心  =====  如果负载已经到4  说明已经繁忙
cpu  2核心  =====  如果负载已经到2  说明已经繁忙

cat /proc/loadavg 
	0.00       0.01         0.05 		1/126 1484
	最近1分钟  最近五分钟	最近15分钟
	
查看负载的命令:
w 查看负载信息 可看 多少用户 运行时间
uptime 查看负载信息
top 动态查看负载信息
```

/proc/mounts  查看挂载信息

```
/proc/mounts  查看挂载信息
```

### 3.系统优化.1

例如:你用的操作系统的版本是多少

```
cat /etc/redhat-release
CentOS Linux release 7.5.1804 (Core) 
```

例如:你用的操作系统的版本是多少

```
uname -r uname -a
3.10.0-862.el7.x86_64
```

命令行

```
[\u@\h \W]\$
u用户 h主机名称 W相对路径 w绝对路径
[root@lkk ~]
```

## day11系统优化 .2

### 1.修改yum源 修改为国内 下载度快

```
/etc/yum.repos.d/  配置yum源目录
/etc/yum.repos.d/CentOS-Base.repo  yum仓库地址 必须得有 如果没有 找不到软件

查看 命令是否存在 which wget
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

yum报错 可执行 
yum clean all	(清空缓存)

```

### 2.创建用户

```
cat /etc/passwd|grep 用户名      查找用户名

useradd xxxx	添加普通用户
查看用户是否存在 id xxxx	cat /etc/passwd|grep xxxx
										筛选
```

### 3.配置密码

```
passwd xxxx
在 普通用户中切换到管理员用户 
方法一:logout
方法二:exit
方法三:ctrl + d

```

### 4.关闭SELinux

```
永久关闭SELinux
/etc/selinux/config
enforcing 表示selinux已经启动状态
permissive 表示selinux关闭状态 会提示警告信息
disabled 表示selinux 关闭状态
关闭selinux 
SELINUX=enforcing	修改为SELINUX=disabled 重启系统生效
getenforce	查看当前状态 selinux 是否开启

临时关闭SElinux
setenforce 0 0是关闭 1是开
```

### 5.关闭防火墙

```
1.临时关闭防火墙	(centos7)
systemctl stop firewalld
/etc/init.d/iptables stop (centos6)
service iptables stop
临时关闭 重启失败
2.如何查看防火墙状态 (centos7)
systemctl status firewalld
iptables-save 没有提示就是最好的提示 没有提示就是关闭状态
(centos6)
/etc/init.d/iptables status
iptables-save
3.永久关闭防火墙 (centos7)
chkconfig --list 查看所有服务是否开机启动
systemctl disable firewalld	(禁止开机启动)
chkconfig iptables off (centos6)

什么时候开启和关闭防火墙 (在公司禁止关闭运行中的防火墙)
1.什么时候开启
	1.你的服务器是公网ip地址
	2.服务器是阿里云的并且对外提供服务的(云服务)
	3.端口:ssh	22	远程连接端口
2.什么时候不开启
	1.内网	办公室	外面的人不容易访问到你的服务器
	2.服务器单纯的测试 内部测试 不安装任何对外服务
	3.高并发访问服务器	(硬件防火墙)
```

提问:pv uv 什么是独立ip

我们的服务器应该说什么样的配置 

web 要什么硬件配置 NFS什么配置 MySQL是什么配置 负载均衡用什么配置

source 让配置文件重新生效 部分配置文件可用

### 6.上传下载

``` 
xshell和CRT上传下载 必须安装lrzsz
xshell 直接拖拽上传 下载使用 sz + 文件名
CRT上传 在命令行输入rz然后选择文件进行上传 sz + 文件名 下载
```

### 7.乱码

```
如果出现乱码问题,需要修改编码,文件编码,或者xshell编码
export LANG="zh_CN.UTF-8"

cat /etc/locale.conf
修改配置文件后重启生效
source /etc/locale.conf
```

## day12 

### seq 序列

```
[root@lkk ~]#seq 4
1
2
3
4
[root@lkk ~]#
```

### less 查看文件*********

```
less 文件名
-N 查看行号
f 往下翻一页 一次翻一页
b 往上翻一页 一次翻一页
/ 搜索 n 查找上一个 N 查找上一个
G 快速到文件尾部
q 退出 ctrl+c 结束
```

### more 查看文件

```
f 往下翻一页 一次翻一页
b 往上翻一页 一次翻一页
到最后自动退出
```

### head* ** **

```
默认显示文件前10行
-n 显示多少行
head -n 2 passwd 显示前两行 简写 head -2 passwd
```

### tail * ** **

```
默认显示文件后10 行
-n 显示多少行
-f 一直显示 简写 tail 监控最新的日志输出
tail -n 2 passwd 显示后两行 简写 tail - 2 passwd
```

### tr 替换** 

```
linux 大部分命令都失直接显示 不更改源文件 安全
/n 换行
-d 删除
-c 取反
cat 文件|tr "xx" "dd"
```

### alias 别名 

```
查看别名
alias
如何设置别名

临时生效
alias rm="echo 我是你爸爸爸"

永久生效
环境变量执行的顺序 坑
/etc/profile 开机执行此文件	重新打开xshell窗口不生效
.bashrc	新开窗口则生效 执行家目录下的环境变量文件 .bashrc .bash_profile

使用场景 可以把长文件设置别名 快速执行 提高工作效率
```

## day13

### 1.grep  

```
grep "xxx" 文件
ll|grep "xxxx"

^ 以什么开头
$ 以什么结尾
< 输入重定向 把后面的文本 输入到前面的命令
* 所有
-v 取反

grep -v "age" 1.txt

案例:
把文本中的空行和# 删除只留下可执行行
egrep -v "#|^$" 1.txt
```

### 2.文件属性

文件属性 文件和目录查找命令

```
ll -hi ==等同于==ls -lhi
-l long 详细信息
-h 人类可读
-i inode号
```

属性后面的 . 不用管 如果你是开启selinux创建的文件则带 . 关闭后没有

| inode号  | 文件类型 | 文件的权限 | 硬链接个数 | 属主 | 属组 | 文件或目录大小 | 时间  |  文件名目录名   |
| :------: | :------: | :--------: | :--------: | :--: | :--: | :------------: | :---: | :-------------: |
| 33596923 |    -     |  rw-r--r-  |     1      | root | root |       41       | 17:04 |      2.txt      |
| 33574978 |    -     | rw-------  |     1      | root | root |      1.5K      | 03:40 | anaconda-ks.cfg |
| 17382074 |    d     | rwxr-xr-x  |     2      | root | root |       6        | 20:54 |      back       |
| 33596876 |    -     | rw-r--r--  |     1      | root | root |      1.8M      | 21:13 |    CentOS_7     |

```
inode index node 相当于 inode里存放着 文件的属性 block
硬链接 相当于 文件有多少个入口 (可以理解为复制了一份)
时间 创建时间 最后的访问时间 修改文件的时间
第一列 inode index node 索引节点
inode 称为index node 索引(目录)节点,
他的作用是存放文件的属性信息以及block的位置(指向文件的实体指针)
如何查看当前系统总共和使用了多少个inode
df -i
第二列的第一列
文件类型
查看文件类型
file 文件或者目录
- 普通文件
d 目录directory
l 软连接 softlinks 字符连接 *****
b 块设备 block
c 字符设备 character special
s socket设备
p 管道


```

![1557913666031](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1557913666031.png)

## day14  压缩命令

### 1.压缩包格式

```
tar	常用
zip	常用
bz	不常用
bz2	不常用
z	不常用
```

### 2.压缩命令 格式 及其参数

```
tar 压缩包名称 文件 可跟多个目录或文件
注意:压缩最好是在相对路径 如果不是相对路径 需要加参数P
如果不加参数报错 tar: Removing leading `/' from member names
tar  0516.tar.gz     1.txt       /stu/
tar	 压缩完成后的		要压缩的	要压缩的
如果不指定位置 压缩完成后 压缩包存放在当前目录下
如果要压缩到别的目录下 跟上路径

参数 必须以.gz结尾 文件名 all.tar.gz all自定义
tar zcvf all.tar.gz /etc oldboy/oldboy.txt	参数可以简写 zcf
-z 使用gzip压缩
-c 创建
-v 显示压缩过程
-f 指定压缩文件 必须跟在所有参数的后面
-t 查看压缩包内的内容
-C 指定解压到哪里
-x 从压缩包中提取文件

查看压缩包的内容 只能查看包含的目录和文件 内容无法查看 
tar -ztf all.tar.gz
指定压缩的位置
tar zxvf all.tar.gz /etc oldboy/xxxx.txt
解压压缩包
tar zxvf all.tar	参数简写 xf
解压到指定目录
tar zxvf all.tar.gz -C /tmp/

zip压缩相对简单
压缩成zip
zip all.zip xxxx.txt
解压zip文件
unzip xxx.txt
```

### 3.文件的属性

```
Linux下所有的目录和文件全部都是9位权限位
注意:所有和网站相关的目录 目录全部都是755权限 文件全部都是644权限
例:
8192 drwxr-xr-x 2 root root  19 May 16 10:34 /etc/hosts
d rwx r-x r-x
三位为一组
r read 可读 4
w write 可写 2
x execute 可执行 1
属主 (创建者权限) 属组(家庭) 其他用户
```

### 4.软连接和硬链接

```
硬链接
	什么是硬链接:
		如果一个系统文件中（同一个分区）多个文件具有相同inode号 则所有文件互为硬链接
		硬链接相当于给文件增加了一个入口
		相当于超市的多个入口
	创建硬链接
	ln 源文件 目标文件（可改名）
		
	硬链接的作用:
		如果修改源文件或者硬链接文件 则所有的文件都会修改
		文件备份 增强重要文件的安全 删除其中一个硬链接 只是删除了指向 （相当于堵死了一个超市大门）
删除硬链接:
如果只删除源文件 不删除硬链接文件 则文件不会被删除
只有删除源文件和所有的硬链接文件 则文件被删除
如果普通文件（没有被系统调用的文件）硬链接数0 则被删除 如果只删除源文件或者硬链接 则只是删除了inode指向 没有真正的删除block
只有删除源文件和所有的硬链接 则真正意义上删除了block 文件被删除

硬链接显示为普通文件 可直接使用rm 删除
当所有的硬链接文件及源文件被删除后，再存放新的数据会占用这个文件的空间，或者磁盘fsck检查的时候，删除的数据也会被系统回收。
```

```
软链接
	  软连接是一个普通文件 以l 开头  文件内存着源文件的路径  相当于windows的快捷方式
	  创建软链接
```

## day15 通配符跟特殊符号

### 1.首先回顾一下我们用过的"特殊符号"

```
.. #当前目录的上一级目录
! #find与awk中表示取反,linux 命令行表示历史记录
? #任意一个字符,通配符
# #表示注释
| 管道符
$ 以什么什么结尾(正则)
^ 以什么什么开头(正则)
~ 当前用户的家目录
`` 反引号 $() 引用命名的结果
&& 并且 同时成立 前面的命令执行成功  则执行后面的命令
[] 表示一个范围 即合 [abcdefg][a-g](正则,通配符)
{} #产生一个序列
```

### 2.管道命令使用 与 | xargs

xargs 通常与find 结合使用

[特殊符号与通配符](http://note.youdao.com/noteshare?id=86d83d62cd3ba0ac6ccc0a707b86e68a)

## day16  正则表达式

### 1.首先我们要知道什么是正则表达式

- **正则表达式**就是为了处理大量的**文字|文本|字符串**而定义的一套规则和方法。
- 通过定义的这些**特殊符号**的辅助，系统管理员就可以快速过滤，替换或输出需要的**字符串**。
- Linux正则表达式**一般以行为单位处理**的。

### 2.正则表达式分类

- 基础正则表达式(BRE basic regular expression)
  - ^ $ . *  [] [^]
- 扩展正则表达式(ERE extended regular expression)
  - | + ()  {}  ?

3.使用正则表达式注意事项

- Linux正则表达式一般以行为单位处理的。
- 注意特殊符号 中英文
  - ​    ‘’ “” （） 。 *  …… ￥ |  {}  【】
  - ​    ''  ""   ()   .    *      ^   $  |  {}    []

### 4.基础正则

```
1. ^ 以什么什么开头的行	^m 以m开头
2. $ 以什么什么结尾的行	m$ 以m结尾
3. ^$ 空行	什么符号都没有
4. . 任意一个字符	不会匹配空行
5. \ 撬棍	转义字符 还原本质
6. * 前一个字符连续出现0次或者0次以上 1 	1111
7. .* 所有 任何符号 包含空行
	 .*所有符号 任何符号 连续出现的字符  有多少匹配多
	 正则表达 所有符号 或 连续出现 会表现出贪婪性 
8. [][abc] 相当于是一个符号(每次匹配1个字符)找出包含a或b或c
	()小括号
	[]中括号[a-z][A-Z][0-9][0-Z]
	{}花括号
9. [^][^abc] 相当于一个符号 ^在括号中代表取反的意思 (每次匹配一个字符)    找出除了a或b或c

```

### 5.扩展正则

```
1. + 前一个字符连续出现1次或1次以上
2. | 或者的意思
3. () 表示一个整体 反向引用/向后引用
4. {} 0{n,m} 前一个字符至少连续出现n次,最多连续出现了m次
5. ? 前一个字符连续出现0次或1次
```

### 6.基础正则与扩展正则的区别

- 符号不同
- 支持命令的不同
  - BRE	grep/sed/awk
  - ERE        egrep/grep-E/sed-r/awk

### 7.正则总结按照功能划分

|                   基本                   | 表示重复 连续重复 |
| :--------------------------------------: | :---------------: |
|                    ^                     | 表示重复 连续出现 |
|                    $                     |         +         |
| ^$空行 cat -A 取出文件中的空行或空格的行 |         *         |
|                    .                     |      a{n,m}       |
|                    \                     |         ?         |
|                  [^abc]                  |                   |
|                 \| 竖线                  |                   |
|                    ()                    |                   |

### 8.基础正则操作实例

```
在id.txt里编辑以下内容
/oldboy/id.txt
李 2113421234
张 500224197
王 1233423423432oldboy
万 5oldboy
吕 lzy235872451234814
孔 150000123874591242
夏 222113859123487192
赵 37142518322922103X
取出文件中正确的身份证号码的行
身份证特征
身份证18位 最后一位 数字或者是X
```

1. 查找以文件内以m开头的行

   ![1558342686522](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558342686522.png)

2. 查找以文件内以m结尾的行 注意: 必须以...结尾 包括空格

   ![1558342982339](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558342982339.png)

3. 使用cat –A 在文件的末尾显示$

   ![1558343044909](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558343044909.png)-A 在文件末尾显示$ 把tab键显示为^I

4. 查找文件的空格

   ![1558343288525](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558343288525.png)

5. 如果前面是$ 则表示变量 加双引号则会解析变量内容

   ```
   我们定义一个变量 m=lizhenya
   看下各种执行结果 看单引号跟双引号的区别
   ```

   grep '$m ' oldboy.txt

   ![1558343819938](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558343819938.png)

   grep "$m " oldboy.txt

   ![1558344448073](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558344448073.png)

   注意:在这里双引号 直接查找变量定义的内容 m=lizhenya

6. 过滤文件内的空行 ^$ 表示空行 什么符号都没有 -n 是显示行号

   ![1558344943317](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558344943317.png)

7. 过滤任意的一个字符  (. 任意一个字符 不会匹配到空行)

   ![1558345877203](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558345877203.png)

8. grep 查看执行过程 grep每次匹配到了什么

   grep -o '.' oldboy.txt 找到了什么

   ![1558346016065](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558346016065.png)

9. 查找文件内以 . 结尾的行

   grep '  \ .'  oldboy.txt	.是正则里边的字符 要还原.的本意 用\

   ![1558350433444](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558350433444.png)

10. *前一个字符连续出现了0次或0次以上

    ![1558350818691](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558350818691.png)

    4*出现0次会显示所有的内容

    ![1558351063299](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558351063299.png)

11. .* 所有 任何符号 包含空行

    正则的贪婪匹配 .* 所有符号 任何符号 连续出现的字符 有多少匹配多少  (任意字符开头 找到一行内的m 然后结束)

    ![1558352033711](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558352033711.png)

    ![1558352187938](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558352187938.png)

    以m开头 然后到行尾 任意字符

12. [][abc]相[] [abc] 相当于是一个符号 每次匹配一个字符 找出包含a或b或c 一次找一个字符

    grep '[abc]' oldboy.txt

    grep '[abc]' oldboy.txt -o	可以查看执行过程

    [a-z] [A-Z] [0-Z] [0-9] 

    ![1558352432362](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558352432362.png)

13. 在空括号里的字符 大部分没有什么特殊的含义 写什么找什么

    测试 . ! 结尾

    grep '[.!]$' oldboy.txt

    ![1558352672891](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558352672891.png)

    找.!$

    ![1558353580826](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558353580826.png)

14. 括号 () [] {}

    ^ [^]  [ ^ abc] 取反 排除a 排除b 排除c 中括号默认不匹配空行

    ![1558354386255](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558354386255.png)

15. grep '[ ^ ^ $ ]' oldboy.txt

    ![1558354878927](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558354878927.png)

    在括号中第一个^ 代表取反的意思 第二个^就是本意 $也是本意

### 9.扩展正则操作实例

1. 前一个字符连续出现1次或1次以上

   []与+结合使用 过滤 连续的内容

   注： 支持扩展正则要使用 egrep 或者 grep –E

   ![1558355426982](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558355426982.png)

2. 取出连续出现的字母 显示过滤执行过程 –o

   ![1558355485749](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558355485749.png)

   ![1558355700576](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558355700576.png)

3. 过滤连续的a-z字母 显示执行过程 –o 显示的是单词形式 空格区分 连续的字符 使用+

   ![1558355763004](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558355763004.png)

   ![1558355778730](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558355778730.png)

4. | 扩展正则表达式 或者的意思

   ![1558355851517](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558355851517.png)

   /etc/ssh/ssh_config 排除文件中的#和空行

   ![1558355914805](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558355914805.png)

5. {} 第一种方法 {n,m} 前一个字符至少连续出现n次 最多出现m次

   ![1558356200719](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558356200719.png)

   ![1558356213721](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558356213721.png)

6. 第二种方法8{3}最多显示多少次

   ![1558356405366](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1558356405366.png)


## day17 三剑客grep+sed

### 1.grep参数`

```
-E 支持扩展正则 *****
-c 显示过滤内容的行数
-n 过滤出的内容加行号
-v 取反 *****
-o 只过滤我们想要的内容 查看匹配到了什么 *****
-i 不区分大小写
-A 查看当前行的下一行 -A 2
-B 查看当前行的上一行 -B 2
-C 查看当前行的上下n行 -C 2
-w 取出单词 *****
-r -R 递归查询内容
```

### 2.sed使用方法及其参数

#### 1.语法格式

```
sed [options] [sed-commands] [input-file]
sed [选项]     [sed命令]      [输入文件]
说明：
1. 注意sed软件以及后面选项、sed命令和输入文件，每个元素之间都至少有一个空格。
2. 为了避免混淆，本文称呼sed为sed软件。sed-commands(sed命令)是sed软件内置的一些命令选项，为了和前面的options(选项)区分，故称为sed命令。
3. sed-commands既可以是单个sed命令，也可以是多个sed命令组合。
4. input-file(输入文件)是可选项，sed还能够从标准输入如管道获取输入。
```

#### 2.选项说明

| options[选项] |                   解释说明（带※的为重点）                    |
| :-----------: | :----------------------------------------------------------: |
|      -n       |         取消默认的sed软件的输出,常与sed命令的p连用※          |
|      -e       |               一行命令语句可以执行多条sed命令                |
|      -f       |                选项后面可以接sed脚本的文件名                 |
|      -r       |       使用扩展正则 默认情况sed只识别基本正则表达式。※        |
|      -i       | 直接修改文件内容，而不是输出到终端。如果不使用-i选项sed软件只是修改在内存中的数据，并不会影响磁盘上的文件。※ |

| sed-commands[sed命令] |                   解释说明（带※的为重点）                    |
| :-------------------: | :----------------------------------------------------------: |
|           a           |              追加,在指定行后添加一行或多行文本※              |
|           c           |                   取代指定的行 [替换整行]                    |
|           d           |                        删除指定的行※                         |
|           D           | 删除模式空间的部分内容，直到遇到换行符\n结束操作，与多行模式相关。 |
|           i           |              插入,在指定行前添加一行或多行文本※              |
|           h           |               把模式空间的内容复制到保持空间。               |
|           H           |               把模式空间的内容追加到保持空间。               |
|           g           |               把保持空间的内容复制到模式空间。               |
|           G           |               把保持空间的内容追加到模式空间。               |
|           x           |                交换模式空间和保持空间的内容。                |
|           l           |                       打印不可见的字符                       |
|           n           |               清空模式空间的内容并读入下一行。               |
|           N           |     不清空模式空间，并读取下一行数据并追加到模式空间。※      |
|           p           |        打印模式空间的内容,通常p会与选项-n一起使用。※         |
|           P           |        打印模式空间的内容，直到遇到换行符\n结束操作。        |
|           q           |                           退出sed                            |
|         s###g         | 取代，s#old#new#g==>这里g是s命令的替代标志，注意和g命令区分。※ |
|           w           |                 把模式空间的内容保存到文件中                 |
|           y           |                     根据对应位置转换字符                     |

| 特殊符号 |             解释说明（带※的为重点）             |
| :------: | :---------------------------------------------: |
|    !     |         对指定行以外的所有行应用命令。※         |
|    =     |                打印当前行行号。※                |
|    ~     | “First~Step”表示从First行开始，以步长Step递增。 |
|    &     |               代表被替换的内容。                |
|    ;     |     实现一行命令语句可以执行多条sed命令。※      |
|    {}    |       对单个地址或地址范围执行批量操作。        |
|    +     |       地址范围中用到的符号，做加法运算。        |

## day18 三剑客awk

### 1.awk使用方法及其参数

```
1.什么是awk?
    awk 是一种编程语言，用于在`linux/unix`下对文本和数据进行处理。
    awk 数据可以来自标准输入、一个或多个文件，或其它命令的输出。
    awk 通常是配合脚本进行使用, 是一个强大的文本处理工具。
2.awk怎么处理数据?
	进行逐行扫描文件, 从第一行到最后一行
	寻找匹配的特定模式的行,在行上进行操作
	如果没有知道处理动作,则把匹配的行显示到标准输出
	如果没有指定模式,则所有被操作的行都被处理
3.awk的语法格式
    awk [options] 'commands' filenames 
    awk [options] -f awk-script-file filenames
4.执行动作
	NR==1	表示用来表示第一行 NR表示行
	NF		NF表示这一行有多少列 $NF 就是输出最有一列
	{print }	输出执行动作
	$	输出变量
5.awk 取范围 awk 判断 关系运算符
    ==
    !=
    >=
    <=
    >
    <
 6.算术运算符
 	+
 	-
 	*
 	/
 7.正则运算符 匹配正则表达式和不匹配正则表达式
 	~
 	~!
 8.其他
 	$ 输出变量
```

### 2.awk参数

| 选项参数 |           解释            |
| :------: | :-----------------------: |
|    -F    | 指定分隔符 默认以空格分隔 |
|          |                           |
|          |                           |
|          |                           |
|          |                           |

### 3.awk实例

```
取出文件中倒数第二列
awk '{print $(NF-1)}' /var/log/secure
```

## day19系统启动流程

### 1.Centos6启动流程

|             centos6启动流程             |            解释             |
| :-------------------------------------: | :-------------------------: |
|                按下电源                 |    远控卡 启动 重启 关机    |
|             (BIOS)开机自检              | cpu 内存 磁盘硬件是否有问题 |
|                 MBR引导                 |        通过硬盘启动         |
|                GRUB菜单                 |  选择不同内核 C6单用户模式  |
|              加载内核 boot              |         加载到内存          |
|              运行INIT进程               |      第一个启动的进程       |
|            读取/etc/inittab             |          运行级别           |
|           读取/etc/rc.sysinit           |         初始化系统          |
| 根据运行级别运行/etc/rc数字.d下面的脚本 |        开机自动启动         |
|   启动mingetty显示登录界面 运行login    |       匹配/etc/passwd       |

### 2.centos7启动流程

|                       centos7启动流程                        | 执行解释 |
| :----------------------------------------------------------: | :------: |
|                           按下电源                           |          |
|                        (BIOS)开机自检                        |          |
|                           MBR引导                            |          |
|                           GRUB菜单                           |          |
|                           加载内核                           |          |
|                           systemd                            |          |
|                           target 7                           |          |
| 并行启动/usr/lib/systemd/system<br />/etc/systemd/system 服务 |          |
|                    启动login显示登陆界面                     |          |
|                                                              |          |

```
centos6 centos7 启动流程的区别
centos6 串行启动 执行完一个在执行下一个
centos7 并行启动 一起执行
```

### 3.用户相关

```
用户UID 
centos6 UID 从500开始 包含500
centos7 UID 从1000开始 包含1000
默认创建的用户每次加1
```

```
passwd 行内解释
root:	x:	  0:	0:	 root:        /root:		/bin/bash
用户名  密码 UID    GID  用户的说明   家目录           命令解释器
/etc/skel 新用户的老家的模板（新用户家目录的样子） 存放新建用户的目录（创建新用户后会把这个目录下的文件拷贝到新用户的家目录下）
```

```shell
面试题: 登录到普通用户下突然发现
-bash-4.2$ 
-bash-4.2$ 
-bash-4.2$ 
-bash-4.2$ 

解决 拷贝skel下所有的文件到当前家目录
解决方法
-bash-4.2$ source .bash_profile   直接source  或者重新退出登录

注意:
	不要去其他普通用户家里拷贝 没有权限 拷贝/etc/skel/
	
注意. 空格  代表source 重新加载 加上. 隐藏文件 .bashrc
[root@oldboyedu ~]# . bash*
-bash: bash*: No such file or directory
[root@oldboyedu ~]# . .bash
.bash_history  .bash_logout   .bash_profile  .bashrc        
[root@oldboyedu ~]# . .bashrc 
[root@oldboyedu ~]# source .bashrc
```

#### 1.useradd 使用及其参数

```
useradd
-u 指定用户的uid(数字 身份证号)
-s 指定用户使用的shell
	/bin/bash
	/sbin/nologin 手动添加一个虚拟用户 不让其登录
-M 表示不创建家目录 一般创建虚拟用户使用
-g 指定用户的组 (租的名字) 添加用户的时候默认
	创建一个与用户名一样的家庭
```

#### 2.userdel 删除用户

```
userdel 工作中如果要删除用户 在passwd中注释掉
userdel 默认不删除用户的家目录和邮箱
-r 连窝端删除与用户有关的所有信息 (家目录)
```

#### 3.usermod 修改用户信息

```
usermod 修改用户信息
-s 修改用户使用的shell
-g 属于的家庭 用户组的名字 主要组
-G 属于多个家庭 附加组
```

## day20用户相关

## day21权限体系

### 1.练习题回顾:批量创建用户添加随机密码

```
一 批量添加用户执行过程 (sed)
1. date +%N%S|md5sum|cut -c1-8 用时间生成MD5秘钥 但是秘钥太长了 用cut取前8位
2. pass=`date +%N%S|md5sum|cut -c1-8` 这里要定义变量 把后边的执行结果赋值给pass
3. echo lkk{1..10} |xargs -n 1|sed -r 's#(.*)#useradd \1;pass=$(date +%N%S|md5sum|cut -c1-8);echo $pass \1 >> pass.txt;echo $pass|passwd --stdin \1#g'|bash
4. echo lkk{1..10} 这段执行的是输出 lkk1..lkk10
5. xargs -n 1 向后引用变成一列 如果是一行的话不能创建10个用户了 一行就是一个了
6. sed -r 's#(.*)#useradd \1 -r扩展正则 这段的执行结果就是 useradd lkk1 
7. pass=$(data +%N%S|md5sum|cut -c1-8) 定义变量 
8. echo $pass \1 > pass.txt 把密码账号 导入到pass.txt
9. echo $pass|passwd --stdin \1 免交互设置密码
二 批量删除用户
echo lkk{1..10} |xargs -n 1|sed -r 's#(.*)#userdel -r \1#g'|bash
```

### 2.权限体系

```
1.文件对应权限 
    r===read 可读
    w===wirte 可写
    x===execute 可执行

    如何查看当前用户对某个文件或目录有什么权限
    ll /etc/hosts
    -rw-r--r-- 等信息
2.权限的计算
	r===4
	w===2
	x===1
	-===0
```

### 3.修改文件权限 chmod

```
属主	属组		其他用户
user	group	others
u		g		o
方法一:
	例:属主去掉写入权限使用
	chmod u-w 1.sh
	例:属主同时给rwx权限 = 先清除在赋值权限
	chmod u=rwx 1.sh
	例:属主 属组 其他用户同时增加x执行权限
	chmod ugo+x 1.sh
	例:同时删除x执行权限 a=ugo (a=all)
	chmod a-x 1.sh
	例:使用a对所有用户增加x执行权限
	chmod a+x 1.sh
方法二:使用数字进行权限修改(常用)
	例: 文件修改为644权限 rw-r--r—
	chmod 644 1.sh
	例：文件修改为755权限 rwx-r-xr-x
	chmod 755 1.sh
```

### 4.文件权限总结

1. r 显示文件内容
2. w 是否能修改文件的内容 需要有r权限配合 只有w的时候 保存退出会导致文件内容清空
3. x是否有执行权限 需要有r可读权限配置

### 5.目录权限总结

1. r是否能查看目录的内容ls
2. w是否能在目录中创建 删除 重命名 文件权限
3. x 是否有进入目录的权限 cd
4. 注意(目录权限):
   1. r 是否能查看目录内容ls 需要x权限配合
   2. w 是否能在目录中创建删除 重命名 文件权限 需要x权限配合
   3. .x 是否能进入到目录权限 cd 是否能修改或查看目录中文件属性

![day21æéä½ç³"ç"ä¹ ](https://www.linuxnc.com/wp-content/uploads/2019/05/word-image-269.png)

![day21æéä½ç³"ç"ä¹ ](https://www.linuxnc.com/wp-content/uploads/2019/05/word-image-272.png)

## day22 文件隐藏属性定时任务

### 1.文件的隐藏属性

#### 1.chattr

```
赋值文件隐藏属性
chattr +a ***.txt	只能写入 追加 不可删除 不可修改
chattr -a ***.txt	取消隐藏属性
chattr -i ***.txt	无法修改 无法删除 无敌的存在 给重要的文件增加-i隐藏属性 
```

#### 2.lsattr

lsattr	查看文件属性

![1559050174277](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559050174277.png)

#### 3.suid setuid

suid setuid	s权限位4755

给命令增加s权限 所有用户均为root 相当于root在执行

chomod u+s /bin/rm	(慎用)

#### 4./tmp/ t权限位为1777

任何人都可以删除/tmp里边的内容

chmod o+t /tmp/

添加t权限后只能自己删除自己

![1559050399979](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559050399979.png)

### 2.定时任务配置文件

```
crond 定时任务软件 (cronie软件包)
系统定时任务配置文件:/etc/crontab
用户定时任务配置文件所在目录:/var/spool/cron (目录 ls命令查看)
定时任务的日志文件 运行过程的记录:/var/log/cron
```

### 3.定时任务命令以及参数

```
crontab -l	查看当前用户的定时任务
crontab -e	写入当前用户定时任务
systemctl status crond	查查当前是由正在运行定时任务
systemctl is-active crond	同上
>/dev/null 2>&1 执行的过程直接扔进黑洞或者文件内 否则会导致硬盘inode空间沾满或者邮箱开启/var/spool/mail/root
```

### 4.定时任务常用符号

```
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
 分  时 日  月 周

*	代表 每
00	代表整点
00-23	时
00-59	分
*/10	每隔10分钟
```

### 5.注意

```
定时任务中超过两条命令 先写入脚本.sh 之后直接定时执行脚本
同意脚本的位置 /server/scripts
定时任务时 命令使用全路径 which查看命令所在路径
为定时任务规则加必要的注释
在执行的shell脚本前加上/bin/shell
----------------------------------
定时任务中命令或脚本的结尾加>/dev/null 2>&1
定时任务如需打印日志，则可以追加到指定的日志文件。定时任务计划脚本结尾最好不要留空，因为默认情况下，定时任务每执行一次后，都会给对应的用户发邮件。就可能会导致邮件临时目录文件数猛增的隐患发生，大量小文件占用inode节点数量。
>/dev/null 2>&1 中>表示重定向，/dev/null 为特殊设备文件（黑洞），2>&1表示让标准错误和标准输出一样。本命令的意思是把前面脚本的正常和错误输出都重定向到/dev/null
```

## day24 磁盘管理

### 1.查看磁盘分区

```
fdisk -l

执行内容
Disk /dev/sda: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x000f01e2

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      411647      204800   83  Linux
/dev/sda2          411648     2459647     1024000   82  Linux swap / Solaris
/dev/sda3         2459648    41943039    19741696   83  Linux

Disk /dev/sdb: 106 MB, 106954752 bytes, 208896 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x49500bb6

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048      208895      103424   83  Linux

```

### 2.磁盘分区

```
例如:
fdisk /dev/sdb
fdisk可选择参数
m	查看帮助
n	创建一个新的分区
p	查看分区
d	删除分区
w	保存分区并退出
```

### 3.磁盘格式化

```
mkfs.xfs /dev/sdb1

执行成功
meta-data=/dev/sdb1              isize=512    agcount=4, agsize=6464 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0, sparse=0
data     =                       bsize=4096   blocks=25856, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=855, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
```

### 4.实现开机自动挂载

```
方法一:
vim /etc/rc.local
写入 mount /dev/sdb1 /data
然后给rc.local 加执行权限
chmod +x /etc/rc.local

方法二:
vim /etc/fstab
编辑我们看到我们得获取设备的 UUID号
blkid -s UUID /dev/sdb1
获取/dev/sdb1 的uuid
按照格式写入
UUID=c0197450-93f0-4975-9ac4-38ed2eafb9b9 /data                   xfs     defaults        0 0
```

### 5.通知内核有新磁盘

```
通知内核有新磁盘就不用重启了
partprobe /dev/sdb
```

### 6.扩展

#### 1.Linux下配置一条默认的网关

```
route add default gw 10.0.0.254	添加默认网关
route del default gw 10.0.0.254	删除默认网关
```

#### 2.配置一条静态理由

```
ip route add 0/0 via 10.0.0.254
ip route del 0/0 via 10.0.0.254
```

#### 3.策略路由

```
在centos下使用vpn的情况下安全的
给 test表设置一个默认路由
ip 
ip route add 0/0 via 10.0.0.254 table test
ip rule add from 10.0.0.1 via table
```

## day25 磁盘管理

### 1.开机自动挂载  写入/etc/fstab

![1559463485109](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559463485109.png)

​	如果要把设备自动挂载写入到/etc/fstab 首先要知道设备的UUID

​	查看UUID

​	lsblk -f

​	![1559463649065](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559463649065.png)

​	blkid 查看设备UUID

​	![1559463808921](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559463808921.png)

### 2.使用parted命令给磁盘分区

fdisk 适用于小于2TB的磁盘 fdisk只支持mbr分区表

parted 一般用于大于2TB的磁盘 gpt支持更大的磁盘且支持更多的分区 不写入内存直接生效

### 3.使用parted命令实战

使用命令进入磁盘分区 parted /dev/sdd

![1559464388267](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559464388267.png)设置分区表格式 p ![1559464635894](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559464635894.png)创建分区mkpart primary 0 100![1559464736372](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559464736372.png)![1559464750133](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559464750133.png)rm 删除分区![1559464762781](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559464762781.png)非交互式使用parted 分区创建分区parted /dev/sdd mkpart primary 0 100![1559464836310](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559464836310.png)删除分区parted /dev/sdd rm 1![1559464847460](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559465042465.png)

### 4.添加swap分区

free -h 查看当前的swap分区

![1559465281429](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559465281429.png)

创建一个200M的文件

dd if=/dev/zero of=/tmp/200m bs=1M count=200

![1559465316162](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559465316162.png)

/dev/zero 白洞 不断输出内容

/dev/null 黑洞 没用的东西可以放进去

If=/dev/zero

If=inputfile     /dev/zero 白洞，输入

Of=outputfile   /tmp/200m  输出到/tmp/200m的文件中      

bs=1M  大小为1M   

count=200 复制200次。  

把文件配置为swap，相当于格式化

![1559465881179](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559465881179.png)

激活swap

![1559465893003](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559465893003.png)

可以看到swap空间增大了0.1G

查看swap组成

![1559465909386](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1559465909386.png)























![1556501273070](C:\Users\86186\AppData\Roaming\Typora\typora-user-images\1556501273070.png)

# 企业面试题

1. buffer与cache的区别?

- buffer 写入数据到内存,这个内存数据的空间,称为缓冲区

- cache 从内存读取数据,这个存数据的内存空间,称为缓存区

2. 你用过的服务器型号有哪些?配置如何?

- DELL R720 2U 双路 16个SAS硬盘 32G 64G 一个cpu 4核16G

3. 程序,进程,守护进程的区别

- 程序:静态的 放在硬盘里边的视频 图片
- 进程:播放视频 运行中
- 守护进程:不间断运行在后台 或成为服务

# 单词

|              单词              |      意思      |
| :----------------------------: | :------------: |
|       software selection       |    软件选择    |
|        minimal install         |    最小安装    |
|    installation destination    |     目的地     |
| automatic paritioning selected | 已选择自动分区 |
|       standard partition       |    标准分区    |
|              swap              |    交换分区    |
|         Accept Changes         |    接受改变    |
|            general             |     一般的     |
|            torrent             |      种子      |
|            minimal             |      最小      |
|             readme             |      帮助      |
|             based              |      基于      |
|             input              |      输入      |
|             output             |      输出      |
|              test              |      测试      |
|            install             |      安装      |
|        Troubleshooting         |    故障排除    |
|            continue            |      继续      |
|              data              |      数据      |
|            storage             |      储存      |
|            options             |      选项      |
|          partitioning          |      分区      |
|           configure            |      配置      |
|              will              |     将,要      |
|              LVM               |   逻辑卷管理   |
|            standard            |      标准      |
|             accept             |      接受      |
|             kernel             |      内核      |
|             failed             |      失败      |
|                                |                |
|                                |                |
|                                |                |
|                                |                |
|                                |                |
|                                |                |
|                                |                |
|                                |                |
|                                |                |
|                                |                |

# 命令

|                     命令                      |                             意思                             |
| :-------------------------------------------: | :----------------------------------------------------------: |
|                    ip add                     |                          查看ip地址                          |
| vi /etc/sysconfig/network-scripts/ifcfg-eth0  |                         修改网卡配置                         |
|           systemctl restart network           |                           重启网卡                           |
|                   route -n                    |                           查看网关                           |
| cat /etc/sysconfig/network-scripts/ifcfg-eth0 |                         查看网卡配置                         |
|                      cd                       |                          回到家目录                          |
|                     cd .                      |                           当前目录                           |
|                     cd ..                     |                          返回上一层                          |
|                     cd -                      |                 在最近两次工作的目录来回切换                 |
|                     df -h                     |                       查看磁盘使用信息                       |
|             vim /etc/resolv.conf              |                           修改dns                            |
|           ifdown eth0 && ifup eth0            |                                                              |
|                      pwd                      |                         查看当前路径                         |
|                      man                      |                       manual 手册帮助                        |
|                     type                      |                      查看是否为内置命令                      |
|                      ls                       |                        查看目录的内容                        |
|                   ls-l = ll                   |                   以长格式显示内容详细信息                   |
|                     ls-r                      |                           逆序排序                           |
|                     ll-t                      |                           时间排序                           |
|                  mkdir xxxx                   |                          创建文件夹                          |
|                 mkdir -p xxxx                 |                        递归创建文件夹                        |
|                   shutdown                    |                             关机                             |
|                  shutdown -h                  |                             关机                             |
|                  shoudown -r                  |                             重启                             |
|                shutdown -h 10                 |                         10分钟后关机                         |
|                  shutdown -c                  |                  取消正在执行的关机重启命令                  |
|                     halt                      |                           直接关机                           |
|                shutdow  -h now                |                           直接关机                           |
|                shutdown -r now                |                           直接重启                           |
|                    init 0                     |                             关机                             |
|                   poweroff                    |                             关机                             |
|                     tree                      |                    以树形结构显示目录信息                    |
|                   tree -L 2                   |                历出所有层目录 加数字显示几层                 |
|                    tree -F                    |            在目录的后边加上/ 识别是否为目录或文件            |
|                    tree -d                    |                          只显示目录                          |
|                     touch                     |                      创建文件和更改时间                      |
|               echo "hello word"               |                       把内容输出到屏幕                       |
|                      cp                       |                             复制                             |
|                     cp -r                     |               递归 复制目录下所有的文件和目录                |
|                     cp -p                     |                   复制后保留原始的文件属性                   |
|                     cp -d                     |                链接 (相当于win7 10 快捷方式)                 |
|                     cp -a                     |                         相当于 -pdr                          |
|         cp oldboy.txt /tmp/lidao.txt          |                  复制文件 到tmp目录下并改名                  |
|                      \cp                      |                        不提示是否覆盖                        |
|                      mv                       |     mv 文件或目录 要放到哪里 mv   oldboy.txt   test.txt      |
|                      rm                       |             删除文件或目录 慎用 工作中使用mv代替             |
|                     rm -f                     |                       强制删除 不提示                        |
|                     rm -r                     |                           递归删除                           |
|                      cat                      |                           查看文件                           |
|                    cat -n                     |                        显示文件总行数                        |
|            mount /dev/cdrom /mnt/             |                             挂载                             |
|                umount -f /mnt/                |                             卸载                             |
|                     df -h                     |                       查看磁盘使用信息                       |
|                   hostname                    |                          查看主机名                          |
|                  hostname -I                  |                        查看获取ip地址                        |
|               hostname +主机名                |              修改主机名临时生效 centos 6 7一样               |
|            /etc/sysconfig/network             |                    永久配置主机名 centos6                    |
|       hostnamectl set-hostname +主机名        |             永久修改主机名 centos7 重启系统生效              |
|                 /etc/hostname                 |             centos7 主机名配置文件 重启系统生效              |
|                  /etc/fstab                   |                    开机设备自动挂载的文件                    |
|                    free -h                    |                       查看内存使用信息                       |
|                 /etc/rc.local                 |                开机自动执行此配置文件中的内容                |
|          chmod +x /etc/rc.d/rc.local          |                          给文件权限                          |
|             yum -y install 软件名             |                           安装软件                           |
|               rpm -ivh 包的名字               |        安装rpm包 -i install-v 显示安装过程-h 人类可读        |
|                    rpm -qa                    |          查看所有的安装包 q query 查看 a all 所有的          |
|                     grep                      |                       过滤出想要的内容                       |
|               grep "xxxx" file                |                    在文件中过滤出 xxx内容                    |
|                 ls\|grep xxx                  |                       目录中过滤出xxx                        |
|                yum remove 包名                |                           卸载软件                           |
|               rpm -e 软件包全名               |                           卸载软件                           |
|                     lscpu                     |                         查看cpu信息                          |
|                     free                      |                       查看内存信息 -h                        |
|                      df                       |                     显示磁盘使用信息 -h                      |
|                       w                       |                         查看负载信息                         |
|                    uptime                     |                         查看负载信息                         |
|                      top                      |                         查看负载信息                         |
|           which xxx 例如:which wget           |                  查看命令路径 或者是否安装                   |
|                  getenforce                   |         查看SELinux状态 Enforcing开启 permissive关闭         |
|                 setenforce 0                  |           0是关闭 1是开启 临时关闭SELiux 重启失效            |
|            systemctl status 服务名            |                      查看服务的状态信息                      |
|                 iptables-save                 |          如果出内容 说明开启 不出内容 就是关闭状态           |
|                   uname -r                    |                         查看内核版本                         |
|                   uname -a                    |                         查看内核版本                         |
|                     less                      |                 查看文件 f b翻页 -N 显示行号                 |
|                     more                      |                        查看文件用的少                        |
|                     head                      |              从头部查看 -n 查看多少看 -5 看五行              |
|                     tail                      |                    从尾部查看 -f 动态查看                    |
|                      tr                       |              替换 cat xx.txt \| tr "abc" "123"               |
|                     alias                     |                           设置别名                           |
|              yum provides route               |                     安装命令查找安装包名                     |
|               file 文件名or目录               |                     查看文件或目录的类型                     |
|                  which tree                   |                        查找命令的路径                        |
|                    whereis                    |                查看文件的路径和文件相关的文件                |
|                   find xxxx                   |        查找文件或者目录 -type类型 -name名字 -size大小        |
|                     du -h                     |               显示目录下所有文件的大小和总大小               |
|                    du -sh                     |                       只显示目录总大小                       |
|                     wc -l                     |                            统计行                            |
|                     wc -L                     |                        统计字符串长度                        |
|                    unalias                    |                         取消临时别名                         |
|               grep -E == egrep                |                         支持扩展正则                         |
|                    grep -c                    |                      显示过滤内容的行数                      |
|                    grep -n                    |                      过滤出的内容加行号                      |
|                    grep -v                    |                             取反                             |
|                    grep -o                    |                    只过滤出我们想要的内容                    |
|                    grep -i                    |                         不区分大小写                         |
|                   grep -r R                   |                         递归查询内容                         |
|                    grep -w                    |                           取出单词                           |
|                    sed -n                     |                   取消默认输出 与p结合使用                   |
|                  sed 's###g'                  |                             替换                             |
|                    sed -r                     |                         支持扩展正则                         |
|                    sed -i                     |                        直接更改原文件                        |
|                      3a                       |                  在当前的下一行插入内容内容                  |
|                      3i                       |                   在当前行的上一行插入内容                   |
|                      3c                       |                      替换当前的正好内容                      |
|                 useradd xxxx                  |                           添加用户                           |
|                  useradd -u                   |                       添加用户指定uid                        |
|                  useradd -s                   |            指定登录shell /bin/bash  /bash/nologin            |
|                  useradd -g                   |                           指定GID                            |
|                  useradd -M                   |                         不创建家目录                         |
|                 userdel xxxx                  |                           删除用户                           |
|                  uesrdel -r                   |                          删除家目录                          |
|                  usermod xxx                  |                         修改用户信息                         |
|                  usermod -s                   |                     修改用户使用的shell                      |
|                  usermod -g                   |                属于的家庭 用户组的名字 主要组                |
|                  usermod -G                   |                     属于多个家庭 附加组                      |
|                  passwd xxxx                  |         配置用户名密码 直接输入passwd 是修改root密码         |
|                     chown                     |                     更改文件的属主 属组                      |
|                   chown -R                    |                 递归更改更改文件的属主 属组                  |
|                       w                       |                         查看登录信息                         |
|                     last                      | 显示详细的登录信息  几点登录的 几点退出的 在线多久  查看时间段的日志（分析他干了啥） |
|                    lastlog                    |          显示linux中所有用户最近一次远程登录的信息           |
|                                               |                                                              |
|                                               |                                                              |
|                                               |                                                              |
|                                               |                                                              |
|                                               |                                                              |
|                                               |                                                              |





