

# 综合架构

## day01

### 1.企业上网原理

```
路由器配置：
    第一步：获取设备管理IP地址，修改笔记本网卡地址
    第二步：外网线路配置（运营商），完成路由或拨号配置（用户名 密码--			获取外网IP）
	第三步：配置DHCP服务，实现办公环境主机自动获取IP地址
            IP地址池信息：192.168.19.1~~192.168.19.200
            子网掩码信息：255.255.255.0
            网关地址信息：192.168.19.254
            DNS地址信息：223.5.5.5

交换机配置：
	第一步：配置交换机的vlan （v虚拟 lan局域网）
			一个交换机所有接口属于一个广播域
交换机的接口类型（）：
	01. 属于vlan接口，标记属于哪个vlan
	02. 属于trunk接口，实现不同vlan数据进行传递
```

### 2.虚拟机上网原理

```
NAT:  
	优势：防止IP地址冲突
	劣势：不便于其他物理主机访问虚拟主机
	桥接：
	优势：便于其他物理主机访问虚拟主机
	劣势：容易造成IP地址冲突
仅主机：
	优势：网络环境安全
	劣势：不能访问外网
```

### 3.系统路由配置说明

```
静态网段路由配置：
centos6：net-tools
route
route add -net 192.168.10.0 netmask 255.255.255.0 gw 192.168.1.100
route del -net 192.168.10.0 netmask 255.255.255.0 gw 192.168.1.100

centos7：iproute（推荐）
ip route
ip route add 192.168.10.0/24 via（经由） 192.168.1.100
ip route del 192.168.10.0/24 via（经由） 192.168.1.100

静态主机路由配置：
centos6：net-tools
route add -host 223.5.5.5/32 gw 10.0.0.254

centos7：iproute（推荐）
ip route add 223.5.5.5/32 via 10.0.0.254

静态默认路由：
centos6：net-tools
route add default gw 10.0.0.254

centos7：iproute（推荐）
ip route add default via 10.0.0.254
```

### 4.综合架构阶段

```
1.能力分水岭，架构述职（面试：曾经干过运维，你以前公司架构是什么）
2.学习很多服务（三剑客 vim 配置文件（定时任务 网卡）find w ps df free--监控服务）
3.学习到很多专业术语 
什么是架构？ 多台服务器组合在一起，服务器架构
什么是集群？ 完成相同工作的一组服务器
什么是负载均衡？ 
...
4. 学习服务的排错能力？运维工程师 服务稳定运行 进行架构服务的维护（不能出问题）
自动化部署服务
5.服务的优化操作
```

### 5.综合架构的组成部分

```
1）防火墙服务器		实现访问流量安全控制			iptables（docker）/firewalld
2）负载均衡服务器	实现访问流量调度处理			nginx
3）web服务器		实现处理用户请求        		nginx
4）数据库服务器     实现数据信息存储（字符串）  	mysql
5）存储服务器		实现数据信息存储（图片/附件）   NFS
6）备份服务器       实现数据信息备份				rsync（自动备份 shell+定时任务/sersync实时备份）
7）缓存服务器*      实现数据内存存储                memcache/redis/mongodb
8）批量管理服务器   实现服务器批量管理				ansible
9）监控服务器       实现监控服务运行状态/报警       zabbix
架构完善部分：
1）解决单点问题     实现主备服务器切换              keepalived
2）VPN服务器        远程技术人员身份                pptpvpn （扩展研究）
https://blog.oldboyedu.com/?s=vpn
3）审计服务器       监控内部人员的所有操作          jumpserver（跳板机）
========================================
面试问题：
01. 公司数据如何进行备份
回答：
1）有备份服务器
2）购买云存储设备（安全问题）
3）两地三中心
```

### 6.架构的规划说明：

```
主机名称和IP地址规划  10台服务器 内存--10G
    负载均衡服务器	 lb01           10.0.0.5/24（外网）172.16.1.5/24（内网）
				 lb02           10.0.0.6/24（外网）172.16.1.6/24（内网）
    web服务器         web01          10.0.0.7/24（外网）172.16.1.7/24（内网）
    				web02          10.0.0.8/24（外网）172.16.1.8/24（内网）
   					web03 			10.0.0.9/24（外网）172.16.1.9/24（内网）
    数据库服务器     db01 			10.0.0.51/24（外网）172.16.1.51/24（内网）
    存储服务器       nfs01 			10.0.0.31/24（外网）172.16.1.31/24（内网）
    备份服务器       backup 		10.0.0.41/24（外网）172.16.1.41/24（内网）
    批量管理服务器   m01 			10.0.0.61/24（外网）172.16.1.61/24（内网）
    监控管理服务器   zabbix-server 	10.0.0.71/24（外网）172.16.1.71/24（内网）

系统基础优化部分：
01. 安全优化
    防火墙关闭（永久）
    selinux关闭（永久）
02. yum镜像源
    阿里云镜像源
    清华yum源
03. 优化hosts文件
    cat >>/etc/hosts<<EOF
    lb01     172.16.1.5
    lb02     172.16.1.6
    web01    172.16.1.7
    web02    172.16.1.8
    web03    172.16.1.9
    db01     172.16.1.51
    nfs01    172.16.1.31
    backup   172.16.1.41
    m01      172.16.1.61
    zabbix-server  172.16.1.71
    EOF
   
```

### 7.虚拟主机网络配置：

```
第一个步骤：创建第二块网卡，设置LAN区段
第二个步骤：开启服务器，对第二块网卡进行地址配置
	nmtui--systemctl restart NetworkManager --- systemctl restart network
第三个步骤：清除网卡的mac地址信息和UUID
	sed -i '/UUID/d' /etc/sysconfig/network-scripts/ifcfg-eth[01]
第四个步骤：重启网卡服务
    systemctl stop/disable NetworkManager
    systemctl restart network
```

### 8.虚拟主机的克隆操作

```
第一个步骤：模板主机链接克隆
第二个步骤：需要对克隆好的主机进行配置
1.网卡地址修改
	sed -i 's#200#41#g' /etc/sysconfig/network-scripts/ifcfg-eth[01]
2.修改主机名称
	hostnamectl set-hostname backup
3.建立新的xshell会话
	systemctl restart network
完整快照拍摄工作

PS：克隆好的主机要一台一台启动配置 
```

## day02

### 1.总和架构备份服务

```
1.用于将数据信息进行恢复
2.用于出现问题进行文件信息对比
```

### 2.如何实现备份服务部署(rsync)

```
1.a fast, versatile, remote (and local) file-copying tool
	rsync软件是一个快速，多功能，远程和本地文件备份工具
	rsync软件可以实现全量和增量备份	（优势）
	增量备份是如何实现的
        两种方式：数学算法--数据比对
        第一种：比对文件数据属性信息（默认）
        第二种：比对文件md5数值

2.rsync命令使用
	rsync软件 cp scp rm ls 有的功能 rsync都有
    rsync替换cp：
    cp /etc/hosts /tmp/      --- 备份文件
    rsync /etc/hosts /tmp/   --- 备份文件
    rsync -a /oldboy /tmp    --- 备份目录
    PS：rsync备份目录时
    目录后面有/    oldboy/   将目录下面的数据信息进行备份
    目录后面没有/  oldboy    将目录下面的内容以及目录备份都进行备份
    
    rsync替换scp：远程备份
    scp -rp /etc/hosts  172.16.1.31:/tmp   --- 远程传输备份文件
    rsync -rp /etc/hosts  172.16.1.31:/tmp
    
    rsync替换rm：
    mkdir /null
    rsync -avz --delete /null/ /tmp   --- 将目录中的数据进行删除或者清空
    --delete 无差异同步参数
    
    rsync替换ls：
    rsync /etc/hosts
    -rw-r-xr-x            391 2019/06/11 09:36:15 hosts
```

### 3.rsync命令详细用法说明

```
1.本地数据备份:等价于cp
    Local:  rsync [OPTION...] SRC... [DEST]
    src：需要备份的数据信息
    dest：备份到什么路径中

2.程的方式备份:类似于scp(全量备份)
    Access via remote shell:
    Pull: rsync [OPTION...] [USER@]HOST:SRC... [DEST]
	[USER@]： 以什么用户身份登录到远程主机，默认以当前登录用户身份登录远程主机
	HOST：    远程主机IP地址或者主机名称信息
	SRC：     需要拉取的远程主机数据信息
	dest：    数据保存到本地主机的路径信息
	
    Push: rsync [OPTION...] SRC... [USER@]HOST:DEST
	SRC：     需要推送的本地主机数据信息
	[USER@]   以什么用户身份登录到远程主机，默认以当前登录用户身份登录远程主机
	HOST：    远程主机IP地址或者主机名称信息
	DEST：    数据保存到远程主机的路径信息

3.守护进程方式进行数据传输（远程方式）
    Access via rsync daemon:
    Pull: rsync [OPTION...] [USER@]HOST::SRC... [DEST]
          rsync [OPTION...] rsync://[USER@]HOST[:PORT]/SRC... [DEST]
    Push: rsync [OPTION...] SRC...         [USER@]HOST::DEST
	      src:  需要推送的本地主机数据信息
		  [USER@]: 输入正确认证用户信息
		  HOST：   远程主机IP地址或者主机名称信息
		  ::DEST   指定备份数据的模块信息
		  
          rsync [OPTION...] SRC... rsync://[USER@]HOST[:PORT]/DEST
		  
    1. 可以实现免交互传输数据
    2. 实现远程传输认证功能
    3. 守护进程方式具有配置文件，满足更多企业需求
    4. 控制客户端连接服务端的数量
```

### 4.rsync守护进程部署过程

```
服务端部署流程:
	第一步:确认软件是否安装
	rpm -qa rsync
	yum -y install rsync
	
	第二步:编写配置文件
	vim /etc/rsyncd.conf
-------------------------配置文件---------------------------
	#rsync_config
    #created by HQ at 2017
    ##rsyncd.conf start##

    uid = rsync						#备份目录的管理用户(属主信息)
    gid = rsync						#备份用户的管理目录(属组信息)
    port = 873						#rsync守护进程服务端口号
    fake super = yes				#伪装成管理员 需要有管理员权限
    use chroot = no					#和安全相关的配置
    max connections = 200			#最大连接数
    timeout = 300					#设置连接的超时时间
    pid file = /var/run/rsyncd.pid		#记录进程的号码信息(停止服务/判断服务是否启动)
    lock file = /var/run/rsync.lock		#锁文件,进行连接的限制
    log file = /var/log/rsyncd.log		#服务的日志文件
    ignore errors					#忽略错误,提高传输数据效率
    read only = false				#设置备份目录是否可读可写
    list = false					#?!!?
    hosts allow = 172.16.1.0/24		#白名单(允许指定网段或主机)
    hosts deny = 0.0.0.0/32			#黑名单(拒绝指定网段或主机)
    auth users = rsync_backup		#指定认真过得用户
    secrets file = /etc/rsync.password	#创建一个密码文件(用户名称:用户密码)
    [backup]						#模块信息
    comment = "backup dir by oldboy"	
    path = /backup					#指定备份目录路径信息
-------------------------配置文件---------------------------
	
	第三步:创建备份目录管理用户
	useradd rsync -s /sbin/nologin -M
	
	第四步:创建认证密码信息,并修改权限
	echo "rsync_backup:123456" > /etc/rsync.password
	chmod 600 /etc/rsync.password
	
	第五步:创建备份目录,并修改目录权限
	mkdir /backup
	chown rsync.rsync /backup/
	
	第六步:启动备份服务
	systemctl start rsyncd
	systemctl enable rsyncd
	
	============================
	所有服务端部署流程:
	1.软件部署安装
	2.软件配置文件编写
	3.软件环境准备(目录 文件 用户信息)
	4.将服务进行启动 systemctl start rsyncd/xxx
	5.进行服务程序验证

客户端部署流程:
	第一步:创建密码文件,并修改权限
	echo "123456" > /etc/rsync.password
	chmod 600 /etc/rsync.password
	第二步:实现免交互传输数据
	rsync -avz /etc/hosts rsync_backup@172.16.1.41::backup --password-file=/etc/rsync.password
```

## day03

### 1.rsync命令参数说明

|        参数         |                           参数解释                           |
| :-----------------: | :----------------------------------------------------------: |
|         -v          |               详细模式输出,传输是的进度等信息                |
|         -z          |                 传输时进行压缩以提高传输效率                 |
|         -a          | 归档参数，表示以递归方式传输文件，并保持所有文件属性，等于-rtopgDl |
|         -r          |       对子目录以递归模式,及目录下的所有目录都同样传输        |
|         -t          |                保持文件时间信息不变（mtime）                 |
|         -o          |                       保持文件属主信息                       |
|         -g          |                       保持文件属组信息                       |
|         -p          |                         保持文件权限                         |
|         -P          |              显示同步的过程及传输时的进度等信息              |
|         -D          |                    保持设备文件信息 b s c                    |
|         -l          |                    保留软连接 (没什么用)                     |
|         -L          |               将链接文件源文件内容进行传输复制               |
|         -e          |   使用的信道协议（remote shell），指定替代rsh的shell程序。   |
|  --exclude=PATTERN  |     指定排除不需要传输的文件信息(和tar参数一样)。？？？      |
| --exclude-from=file | 文件名所在的目录文件，即可以实现排除多个文件(和tar参数一样)。？？？ |
|   --bwlimit=RATE    |                           限速功能                           |
|  --delete（慎用）   |     让目标目录SRC和源目录数据DST一致，即无差异同步数据。     |
|                     |                                                              |

```
补充：如何保证备份的数据属主和属组信息不变
方法一：增加模块
	不同用户向不同模块传输数据
方法二：修改配置文件uid信息
    uid = root
    gid = root
    #fake super = yes
    备份目录属主属组需要修改为 root权限
```

### 2.rsync守护进程企业应用

1. 守护进程多模块功能配置

   - 开发人员
   - 运维人员
   - 数据库人员

2. 创建多模块 --- 创建多模块对应目录并授权 --- 重启服务

   ```shell
   [backup]
   comment = "backup dir by oldboy"
   path = /backup
   [devdir]
   path = /devdir
   [dbdir]
   path = /dbdir
   ```

3. 操作实例

   ```shell
   环境准备:
   [root@nfs01 ~]# mkdir /oldboy_dir/oldboy{01..03} -p
   [root@nfs01 ~]# touch /oldboy_dir/oldboy{01..03}/{a..c}.txt
   [root@nfs01 ~]# tree /oldboy_dir/
   /oldboy_dir/
   ├── oldboy01
   │?? ├── a.txt
   │?? ├── b.txt
   │?? └── c.txt
   ├── oldboy02
   │?? ├── a.txt
   │?? ├── b.txt
   │?? └── c.txt
   └── oldboy03
   ├── a.txt
   ├── b.txt
   └── c.txt
   
   需求:
   01. 排除oldboy01目录不要传输备份，排除oldboy03目录中a.txt文件不要传输备份
   实现需求：--exclude=PATTERN
   [root@nfs01 ~]# rsync -avz /oldboy_dir/ --exclude=oldboy01 --exclude=oldboy03/a.txt rsync_backup@172.16.1.41::backup --password-file=/etc/rsync.password
   sending incremental file list
   ./
   oldboy02/
   oldboy02/a.txt
   oldboy02/b.txt
   oldboy02/c.txt
   oldboy03/
   oldboy03/b.txt
   oldboy03/c.txt
   
   sent 381 bytes  received 134 bytes  1,030.00 bytes/sec
   total size is 0  speedup is 0.00
   
   总结：排除的路径信息，需要编写为指定目录的相对路径
   
   常见错误：
   1. 排除参数后面数据信息，不能写绝对路径
   2. 排除参数后面数据信息，不能写相对路径
   
   02. 排除的数据量比较大，有多个目录，多个文件都要排除
   vim exlude.txt 
   oldboy01
   oldboy03/a.txt
   
   [root@nfs01 oldboy_dir]# rsync -avz /oldboy_dir/ --exclude-from=/oldboy_dir/exclude.txt rsync_backup@172.16.1.41::backup --password-file=/etc/rsync.password
   sending incremental file list
   ./
   exclude.txt
   oldboy02/
   oldboy02/a.txt
   oldboy02/b.txt
   oldboy02/c.txt
   oldboy03/
   oldboy03/b.txt
   oldboy03/c.txt
   
   sent 482 bytes  received 157 bytes  1,278.00 bytes/sec
   total size is 24  speedup is 0.04
   
   3) 守护进程来创建备份目录（全网备份项目）
   [root@nfs01 oldboy_dir]# rsync -avz /etc/hosts rsync_backup@172.16.1.41::backup/oldboy/ --password-file=/etc/rsync.password
   sending incremental file list
   created directory oldboy
   hosts
   
   sent 146 bytes  received 72 bytes  436.00 bytes/sec
   total size is 158  speedup is 0.72
   [root@nfs01 oldboy_dir]# rsync -avz /etc/hosts rsync_backup@172.16.1.41::backup/oldboy/oldboy01/oldboy02/oldboy03/ --password-file=/etc/rsync.password
   sending incremental file list
   rsync: mkdir "oldboy/oldboy01/oldboy02/oldboy03" (in backup) failed: No such file or directory (2)
   rsync error: error in file IO (code 11) at main.c(657) [Receiver=3.1.2]
   问题：不能创建多级目录
   
   nfs 数据备份 backup/172.16.1.31/ 
   web 数据备份 backup/172.16.1.7/ 
   db  数据备份 backup/172.16.1.51/
   
   4) 守护进程的访问控制配置
   hosts allow = 172.16.1.0/24
   hosts deny = 172.17.1.0/24
   建议：尽量只使用一种名单
   
   补充：rsyncd.conf文件中全局配置和局部配置
   全局配置：模块以上的配置信息 影响全部模块
   局部配置：模块以下的配置信息 只影响当前模块  
   总结：局部配置优先于全局配置
   
   5）守护进程无差异同步配置
   目录之间实现无差异同步
   rsync -avz /null/ --delete rsync_backup@172.16.1.41::backup 
   PS: 可以快速清空目录中的数据
   文件之间实现无差异同步（没什么用）
   rsync -avz /oldboy/hosts01 --delete rsync_backup@172.16.1.41::backup --password-file=/etc/rsync.password
   
   6）守护进程的列表功能配置
   list = true
   在客户端，显示服务端的所有模块信息
   ```

### 3.rsync备份服务优缺点

1. 优势

   ```
   1.可以实现增量备份
   2.具有守护进程模式(服务端)
     认证管理控制
     实现白名单黑名单安全访问控制
     密码文件信息,可以实现免交互传输数据
     网络连接数量进行限制
   3.实现数据的加密传输 -e
   ```

2. 缺点

   ```
   1.比较擅长传输大文件,传输大量小文件,容易出问题 (需要打包)
   2.传输大文件,有可能会传输中断 --partial 断点续传
   ```

### 4.